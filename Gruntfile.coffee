###
# Gruntfile
# handles building tasks, compiling tasks. (think ant, maven, gradle)
###

exec = require("child_process").exec
path = require "path"
fs = require "fs"
requirejs = require "requirejs"

###
# Globals
###
JS_DIR = "./wp-content/themes/mariasharapova/assets/js"
COFFEE_DIR = "./wp-content/themes/mariasharapova/assets/coffee"
SASS_DIR = "./wp-content/themes/mariasharapova/assets/sass"
ROOT_DIR = "./wp-content/themes/mariasharapova/assets" 

###
# Global fn's
###

compileSass = ->
  console.log "Compiling sass"
  e = exec "compass compile"
  e.stdout.on "data", (data) ->
    console.log data.toString()

watchSass = ->
  e = exec "compass watch"
  e.stdout.on "data", (data) ->
    console.log data
  e.stderr.on "data", (data) ->
    console.log data

###
# Grunt
###
module.exports = (grunt) ->
  grunt.task.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-contrib-requirejs"

  grunt.initConfig {
    pkg: grunt.file.readJSON "package.json"

    ###
    # Build
    ###

    coffee: {
      options: {
        bare: true
      }
      compile: {
        files: [
          expand: true
          cwd: COFFEE_DIR
          src: ["**/*.coffee", "!lib/*.coffee", "!vendor/*.coffee"]
          dest: JS_DIR
          ext: ".js"
        ]
      }
    },

    ###
    # Watching
    ###
    watch: {
      coffee: {
        cwd: COFFEE_DIR
        files: ["**/*.coffee", "!lib/*.coffee", "!vendor/*.coffee"]
        tasks: ["buildCoffee"]
      }
    }
  }

  ###
  # Tasks
  ###

  grunt.registerTask "buildCoffee", ->
    grunt.task.run "coffee"

  grunt.registerTask "build", ->
    compileSass()
    grunt.task.run "buildCoffee"

  grunt.registerTask "build-scripts", ->
    compileSass()
    grunt.task.run "buildCoffee"
    #watch
    watchSass()
    grunt.task.run "watch"

  grunt.registerTask "develop", ->
    # build
    compileSass()
    grunt.task.run "buildCoffee"
    #watch
    watchSass()
    grunt.task.run "watch"

  grunt.registerTask "default", ["develop"]
