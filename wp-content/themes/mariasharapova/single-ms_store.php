<?php
/**
 * Store listing page
 */

get_header(); 

	while ( have_posts() ) : the_post();

	$shopItemIDs = rpt_get_object_relation($post->ID, 'ms_store_item');
	if ( $shopItemIDs ) {
		$args = array(
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'post_type' => 'ms_store_item',
			'post_status' => 'publish',
			'post__in' => $shopItemIDs,
		);

		$shopItems = get_posts( $args );
	}
	
?>
	<section id="contentArticle" class="content-article store" role="main">

		<footer class="article-footer">
			<?php get_template_part('partial', 'share'); ?>
		</footer>


		<?php if( get_field('store_items') == false) { ?>

				<article class="partner-post<?php if( $shopItems ) : ?> has-store<?php endif; ?>">

					<?php if ( get_field('store_banner') ) { ?>

						<div class="store-banner">
							<img src="<?php echo the_field('store_banner');?>" alt="">
						</div>

					<?php
						} 
						if ( get_field('store_logo')) {
					?>

						<div class="store-logo">
							<?php if ( get_field('store_link')) { ?><a href="<?php echo the_field('store_link'); ?>" target="_blank"><?php } ?>
								<img src="<?php echo the_field('store_logo'); ?>" alt="">
							<?php if ( get_field('store_link')) { ?></a><?php } ?>
						</div>

					<?php } ?>

					<header class="article-header">
						<h1 class="article-title"><?php the_title(); ?></h1>
						<?php edit_post_link('Edit post'); ?>
					</header>
					<div class="wysiwyg">
						<?php the_content(); ?>
					</div>
				</article>

				<?php if( $shopItems ) : ?>
					<article class="entry-content store-list">
						<ul class="store-product-listing">
							<?php
								foreach ( $shopItems as $post ) : setup_postdata( $post );
									get_template_part( 'posttype', 'ms_store_item' );
								endforeach;
								wp_reset_postdata();
							?>
						</ul>
					</article>
					<?php endif; ?>

				<?php } else { ?>

				<?php if( $shopItems ) : ?>
					<article class="store-items-only-entry-content store-list">
						<ul class="store-product-listing">
							<?php
								foreach ( $shopItems as $post ) : setup_postdata( $post );
									get_template_part( 'posttype', 'ms_store_item' );
								endforeach;
								wp_reset_postdata();
							?>
						</ul>
					</article>
					<?php endif; ?>



		<?php }; ?>

			

	</section><!-- #contentMain -->

	<?php endwhile; ?>

<?php get_footer();?>