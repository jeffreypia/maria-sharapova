<?php 
	// Store Item post template

	$postBg = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	$postPrice = lm_get('ms_store_item_price');
	$postLink = lm_get('ms_store_item_link');

	if( $postBg ) :
?>
		<div <?php post_class('store-product-item'); ?>>

			<div class="entry-thumbnail">
				<img src="<?php echo $postBg; ?>" alt="">
			</div>
			
			<div class="content-wrapper">
				<div class="entry-date">
					<div class="entry-month"><?php echo get_the_date('M'); ?></div>
					<div class="entry-day"><?php echo get_the_date('j'); ?></div>
				</div><!-- .entry-date -->

				<h2 class="entry-title"><?php the_title(); ?></h2>

				<?php if( $postPrice ) : ?>
				<h3 class="entry-category"><?php echo $postPrice; ?></h3>
				<?php endif; ?>

				<?php if( $postLink ) : ?>
				<div class="button-container">
					<a href="<?php echo lm_get('ms_store_item_link'); ?>" class="read-more" target="_blank">Buy Now</a>
				</div>
				<?php endif; ?>

				<?php edit_post_link('Edit post'); ?>

			</div><!-- .content-wrapper -->

		</div>
	<?php endif; ?>