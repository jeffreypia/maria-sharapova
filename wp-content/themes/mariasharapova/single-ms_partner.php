<?php
/**
 * Partner listing page
 */
$postIcon = lm_get_img_src('ms_partner_icon', 'full');

get_header();

	while ( have_posts() ) : the_post();
		$slideshow = lm_get(gallery_repeatable);
?>
	<section id="contentArticle" class="content-article bio" role="main">

		<?php
			$class = "";
			if( has_post_thumbnail() ) :
		?>
		<figure class="banner">
			<img src="" data-src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" alt="" class="lazy">
		</figure>

		<?php
			else:
				$class="no-banner";
			endif;
		?>

		<article class="entry-content <?php echo $class; ?>">
			<header class="article-header">
				<img class="entry-icon" src="<?php echo $postIcon; ?>" alt="">
				<h1 class="article-title"><?php the_title(); ?></h1>
				<?php edit_post_link('Edit post'); ?>
			</header>

			<?php the_content(); ?>

			<footer class="article-footer">
				<?php get_template_part('partial', 'share'); ?>
			</footer>

		</article>

	</section><!-- #contentMain -->

	<?php get_sidebar('slideshow'); ?>

	<?php endwhile; ?>

	<button id="top" class="top">Back to top</button>

<?php get_footer();
