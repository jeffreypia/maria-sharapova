<?php

// Currently used get ID of most recent featured post and skip it in the main loop
$skipID;

// GET THE GET VALUES
$type =  (isset($_GET["type"])) ? $_GET["type"] : "";
$value = (isset($_GET["value"])) ? explode(',',$_GET["value"]) : NULL;

// VALID TYPES USED FOR VALIDATION
$valid_types = array("category", "post_format", "post_type");

// CHECK IF TYPE IS VALID AND THAT A VALUE IS SET
if(in_array($type, $valid_types) && isset($value)):

	// GET PAGED VALUE
	$paged = get_paged();

	// SETUP THE ARGS ARRAY
	$args = array(
				'posts_per_page' => 14,
				'paged' => $paged
			);

	// DEPENDING ON TYPE CONTINUE BUILDING ARGS ARRAY
	switch($type) {
		case $valid_types[0]:
			$args['category__in'] = $value;
		break;
		case $valid_types[1]:
		//http://wordpress.org/support/topic/post-formats
		//http://wordpress.stackexchange.com/questions/48901/only-get-posts-of-certain-post-formats
			$args['tax_query'] = array(
					        array(
					            'taxonomy' => 'post_format',
					            'field' => 'slug',
					            'terms' => $value[0],
					        )
					    );
		break;
		case $valid_types[2]:
			$args["post_type"] = $value;
		break;
	} 
		
		// START THE QUERY
		$imploded_value = implode("-", $value);
		$name = $type . "_" . $imploded_value . "_" . $paged;
		delete_transient($name);
		$filter_query = get_transient($name);
		
		if (empty($filter_query)) {
			$filter_query = new WP_Query($args);
			// The Loop
			set_transient($name, $filter_query, HOUR_IN_SECONDS);
		}
		
		// OVERRIDE POSTS ARRAY
		$posts = $filter_query->posts;



	// JEFF'S CODE TO BUILD THE TEMPLATE
	if ( $paged == 1) :
		if($value[0] != "ms_store") {
			get_template_part( 'posttype', 'featured' );
			get_template_part( 'posttype', 'hangout' );
			get_template_part( 'posttype', 'twitter-chat' );
			if($value[0] != "ms_ad") {
				get_template_part( 'loop', 'ads' );
			}
			get_template_part( 'loop', 'ms_schedule' );
		} else {
		 
		}
	endif; 
	

			foreach ( $posts as $post ) : setup_postdata( $post ); 

				// if( $post->ID !== $skipID )
					get_template_part( 'posttype', $post->post_type );
			endforeach;


	
?>
<?php  
endif;
?>