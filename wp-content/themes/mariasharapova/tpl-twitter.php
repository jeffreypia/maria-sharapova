<?php
/**
 * Template Name: Twitter Chat Page
 */

get_header(); 

	while ( have_posts() ) : the_post();
		$slideshow = lm_get(gallery_repeatable);
?>
	<section id="contentArticle" class="content-article twitter-chat" role="main">

		<?php if( has_post_thumbnail() ) : ?>
		<figure class="banner">
			<img src="" data-src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" alt="">
		</figure>
		<?php endif; ?>

		<article class="entry-content">
			<header class="article-header">
				<h1 class="article-title"><?php the_title(); ?></h1>
				<time class="article-date" datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php the_date('M j, Y'); ?></time>
				<?php edit_post_link('Edit post'); ?>
			</header>
			
			<?php echo get_the_content(); ?>

			<footer class="article-footer">
				<?php get_template_part('partial', 'share'); ?>
			</footer>

		</article>

	</section><!-- #contentMain -->

	<?php get_sidebar('slideshow'); ?>

	<?php endwhile; ?>

<?php get_footer();