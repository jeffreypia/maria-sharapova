
				<?php
						// Loop of latest blog posts
						$shopItemIDs = rpt_get_object_relation($post->ID, 'ms_store_item');
						if ( $shopItemIDs ) {
							$args = array(
								'posts_per_page' => -1,
								'orderby' => 'menu_order',
								'order' => 'ASC',
								'post_type' => 'ms_store_item',
								'post_status' => 'publish',
								'post__in' => $shopItemIDs,
							);

							$shopItems = get_posts( $args );
						}
				?>
				<?php 
				if( $shopItems ) : ?>
					<article id="storeList" class="entry-content store-list">
						<ul class="store-product-listing">
							<?php
								foreach ( $shopItems as $post ) : setup_postdata( $post );
									get_template_part( 'posttype', 'ms_store_item' );
								endforeach;
								wp_reset_postdata();
							?>
						</ul>
					</article>
					<?php endif; ?>