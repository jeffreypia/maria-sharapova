<?php
	// Loop of Sponsor posts
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'			=> array( 'ms_sponsor' ),
		'orderby'			=> 'menu_order',
	);
	$sponsors = get_posts( $args );

	if( $sponsors ) :
?>
		<div id="sponsors" class="sponsors">
			<h3 class="sponsors-title">Partners:</h3>
			<ul class="slides">
			
<?php
		foreach( $sponsors as $post ) : setup_postdata($post);

			// If thumbnail has not been populated, skip this Sponsor
			if( has_post_thumbnail() ) :
				$postLink = lm_get('ms_sponsor_link');
?>
				<li <?php post_class(); ?> id="<?php echo $post->post_name; ?>">

				<?php if( $postLink ) : ?>
				
					<a class="entry-thumbnail" target="_blank" href="<?php echo $postLink; ?>">
						<?php echo get_the_post_thumbnail(); ?>
					</a>
				
				<?php else : ?>
				
					<div class="entry-thumbnail">
						<?php echo get_the_post_thumbnail(); ?>
					</div>
				
				<?php endif; ?>

				</li>
<?php
			endif;
			
		endforeach;
?>

			</ul>
		</div><!-- #sponsors -->
	<?php endif; ?>
