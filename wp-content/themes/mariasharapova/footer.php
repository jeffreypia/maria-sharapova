<?php
/**
 * The template for displaying the footer
 *
 */
?>

</section><!-- #content -->

<footer id="footer" class="site-footer" role="contentinfo">


	<div class="ajax newsletter">
		<a class="link-newsletter" href="http://mariastage.wpengine.com/newsletter/" target="_blank">Newsletter <br /> Sign Up</a>
	</div>




<?php
	// facebook like
	if (function_exists('mariasharapova_fb_like_count')) :
		// $fblikeCount = mariasharapova_fb_like_count();
		if($fblikeCount) :
			echo $fblikeCount;
		endif;
	endif;



	// twitter count
	if (function_exists('mariasharapova_twitter_follower_count')) :
		$twitterFollowerCount = mariasharapova_twitter_follower_count();
		if($twitterFollowerCount) :
			echo $twitterFollowerCount;
		endif;
	endif;
?>

<div class="instagram-logo test">

	<a class="link-maria-insta" href="http://instagram.com/mariasharapova" target="_blank"></a>
</div>

<?php
	// Show Sponsors on page 1 only
	if ( !get_query_var('paged') ) :
		get_template_part( 'loop', 'sponsors' );
	endif;
?>

<a href="http://www.lightmaker.com" class="lightmaker" target="_blank">Site by Lightmaker</a>

</footer><!-- #footer -->
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/vendor/jquery.royalslider.min.js"></script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  <?php // Stage GA code ?>
  // ga('create', 'UA-59256309-1', 'auto'); 
  <?php // Live GA code ?>
  ga('create', 'UA-19321594-1', 'auto');
</script>

<?php wp_footer(); ?>
<!-- End Google Analytics -->
<?php // Pre-load loader image ?>
</body>
</html>
