var mariasharapova = {
    $body: $('body'),
    $img: $('img'),
    toggleShare: function() {
        $(this).parent().toggleClass('share-open');
    },
    closeShare: function() {
        $('.share-open').removeClass('share-open');
    },
    toggleMenu: function(event) {
        event.stopPropagation();
        mariasharapova.$body.toggleClass('nav-open');
    },
    closeMenu: function() {
        mariasharapova.$body.removeClass('nav-open');
    },
    openOverlap: function() {
        mariasharapova.$body.addClass('overlap-active');
    },
    closeOverlap: function() {
        $('#overlapContent').empty();
        $('#overlap').removeClass('active');
        mariasharapova.$body.removeClass('overlap-active');
        mariasharapova.closeShare();
    },
    handleClickFilter: function(event) {
        event.preventDefault();

        var $title = $('title'),
            $link = $(this),
            filter = $link.attr('href').substr(1);

        // Add active class to menu item
        $link.parent().addClass('active').siblings().removeClass('active');

        // Close menu
        mariasharapova.closeMenu();

        if (filter === "") {
            // If clearing filter, reset URL and scroll to top of page
            History.pushState(null, null, '/');

            $title.text($title.data('orig'));

            $('html, body').animate({
                scrollTop: 0
            });
        } else {
            // Else push filter to URL
            History.pushState(null, null, '/filter/' + filter + '/');

            $title.text($link.text() + ' | ' + $title.data('orig'));

        }
        $(window).trigger('scroll');
    },
    filterPosts: function(filter) {

        // DISABLE FILTER POSTS
        return false;

        if (filter === '') {
            $('#posts .hentry').removeClass('hidden');
        } else {
            $('#posts .hentry').each(function() {
                var $this = $(this);
                if ($this.hasClass('no-filter') || $this.hasClass(filter)) {
                    $this.removeClass("hidden");
                } else {
                    $this.addClass('hidden');
                }
            });
        }
    },
    handleClickAjax: function(event) {
        event.preventDefault();

        var path = this.pathname;
        mariasharapova.closeMenu();

        // Trigger handleStateChange
        History.pushState(null, null, path);

        return false;
    },
    resetPage: function() {

        var $title = $('title');

        mariasharapova.galleryCloseModal();
        mariasharapova.closeOverlap();
        $title.text($title.data('orig'));

        // Filter should remain unless a different filter is chosen
        // 2014-04-25 JP
        // $('#posts .hentry').removeClass('hidden');
        // $('#primaryNavigation .active').removeClass('active')
        // If no substring, replaceState with homepage
        History.replaceState(null, null, '/filter/hentry');
    },
    getURLParameter: function(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    },
    handleStateChange: function() {
        console.log("handle state change");
        var state = History.getState(),
            url = state.hash,
            pathArray = state.hashedUrl.split('/'),
            $container = $('#overlapContent'),
            $title = $('title');

        if (mariasharapova.getURLParameter('image') && $('.overlap-active').length) {
            return false;
        }

        if (pathArray[3] === '') {
            // If no substring, only close overlap & reset title
            mariasharapova.resetPage();



            return;
        } else if (pathArray[3] === 'filter') {

            // filter ajax request
            mariasharapova.filterAjax(pathArray[4], 1);
            mariasharapova.closeOverlap();

                // GOOGLE ANALYTICS
            if (typeof ga !== "undefined") {
                if (url === "/filter/hentry") {
                    url = "/";
                }

                ga('send','pageview', {
                    'page': url
                });

            }

             $title.text($title.data('orig'));

            return;
        } else {

            // Else open Overlay and AJAX in new page content

            // Prevent overflow on body
            mariasharapova.$body.addClass('overlap-active')

            // Open overlap
            $('#overlap').addClass('active');

            // Get everything inside #content div
            var googleAnalyticsURL = url;
            url += " #content > *";

            // Load new content
            $container.css({
                opacity: "0"
            }).load(url, function(response, status, xhr) {
                if (status === 'error') {
                    History.pushState(null, null, '/');
                    return;
                }

                  $hasStore = $('.has-store');
                  if ($hasStore.length) {
                      $hasStore.find('[data-postid]').eq(0).addClass('selected-store');
                  }



                // GOOGLE ANALYTICS
                if (typeof ga !== "undefined") {
                    ga('send','pageview', {
                        'page': googleAnalyticsURL
                    });
                }

                // Set title
                $title.text($container.find('.article-title').text() + ' | ' + $title.data('orig'));

                // Fade in content
                $container.animate({
                    opacity: "1"
                });

                // Re-initialize Sharrre buttons
                mariasharapova.sharrre('#overlap');

                // Initialize slideshow, if present
                if ($container.find('.slideshow').length) {
                    $('.slideshow').flexslider({
                        animation: 'slide',
                        controlNav: false,
                        slideshowSpeed: 4000,
                        useCss: false,
                        slideshow: false,
                        touch: false,
                        animationLoop: false, 
                    });
                }

                // Initialize Photo Gallery & modal
                if ($('#galleryPhotos').length) {
                    mariasharapova.galleryInit();
                }

                // Wrap container around iframe, if present
                if ($container.find('iframe').length) {
                    $container.find('iframe').wrap('<div class="iframe-wrapper">');
                }

                // Initialize Twitter Widget, if present
                if ($container.find('.twitter-timeline').length) {
                    mariasharapova.initializeTwitterWidget();
                }

                // Refresh image cache
                mariasharapova.$img = $('img');

                // Load correct image for viewport width
                mariasharapova.swapImages();

                // Lazy load
                mariasharapova.lazyLoadImages();
            });
        }
    },
    filterAjax: function(filter, paged) {

        // CREATE AJAX URL
        var _ajaxUrl = "/ms_filter/index/" + paged + "/?type=",
            $posts = $("#posts");

        // SET DATA TO POSTS
        $posts.data("filter", filter);
        $posts.data("paged", paged);
        $posts.data("loaded", false);

        switch(filter){
            case "hentry":
                _ajaxUrl += "post_type&value=ms_schedule,ps_facebook,ps_twitter,ps_instagram,post,ms_store";
            break;
            case "social":
                _ajaxUrl += "post_type&value=ps_twitter,ps_facebook,ps_instagram";
            break;
            case "category-foundation":
               _ajaxUrl += "category&value=39";
            break;
            case "category-news":
                _ajaxUrl += "category&value=38";
            break;
            case "category-press":
                _ajaxUrl += "category&value=74";
            break;
            case "format-gallery":
                _ajaxUrl += "post_format&value=post-format-gallery";
            break;
            case "format-video":
                _ajaxUrl += "post_format&value=post-format-video";
            break;
            case "ms_schedule":
                _ajaxUrl += "post_type&value=ms_schedule";
            break;
            case "ms_store":
                _ajaxUrl += "post_type&value=ms_store";
            break;
            case "ms_sponsor":
                _ajaxUrl += "post_type&value=ms_sponsor";
            break
            case "ms_ad":
                _ajaxUrl += "post_type&value=ms_ad";
            break;
            case "ms_partner":
                _ajaxUrl += "post_type&value=ms_partner";
            break;
        }

        // START AJAX REQUEST
        $.ajax({
            type: "GET",
            url: _ajaxUrl,
            dataType : 'html'
        }).done(function(response) {

            // GOOGLE ANALYTICS
            // if (typeof ga !== "undefined") {
            //     ga('send','event','filter',filter);

            // }

            // CHANGE TO TRUE
            $posts.data("loaded", true);

            // CHECK IF PAGED NUMBER IS GREAT THAN 1
            if(paged > 1) {

                // APPEND HTML
                $posts.append(response);

            } else {

                // SET HTML
                $posts.html(response);

                // SCROLL TO THE TOP IF ON A NEW FILTER
                var _scrollTop = (filter === "hentry") ? 0 : $("."+filter).first().offset().top;
                $('html, body').stop().animate({
                    scrollTop: _scrollTop
                });

            }

            // TRIGGER CALLBACK FUNCTION AFTER SETTING FRESH HTML
            mariasharapova.callbackInfiniteScroll();

            // NEED TO CALL THE IMAGE SWAP METHOD
            mariasharapova.swapImages();

            $('#content-slider-1').royalSlider({
                autoHeight: true,
                arrowsNav: true,
                fadeinLoadedSlide: false,
                controlNavigationSpacing: 0,
                controlNavigation: 'bullets',
                imageScaleMode: 'none',
                imageAlignCenter:false,
                loop: true,
                loopRewind: true,
                numImagesToPreload: 6,
                keyboardNavEnabled: true,
                slidesSpacing: 0,
                usePreloader: false,
                autoScaleSlider: true,
                autoPlay: {
                    // autoplay options go gere
                    enabled: true,
                    pauseOnHover: false,
                }
            }).on('click', '.read-more', function() {
                // Track which slide is clicked on in GA
                ga('send', 'event', 'Featured Slider', 'click', 'Slide- ' + $(this).data("slider"));
            });
        });

// $storeItemId = $(".ms_store a");
// $storeItemId.data("postId");

// var _ajaxUrlStore = "/store-items/?id=" + storeItemId,

//     // START AJAX REQUEST
//     $.ajax({
//         type: "GET",
//         url: _ajaxUrlStore,
//         dataType : 'html'
//     }).done(function(response) {
//         $storeItemId.html(response);
//     });


    },
    bottomOfPage: function(){

        var D = document,
        docHeight = Math.max(
            D.body.scrollHeight, D.documentElement.scrollHeight,
            D.body.offsetHeight, D.documentElement.offsetHeight,
            D.body.clientHeight, D.documentElement.clientHeight
        );

        return ($(window).scrollTop() + $(window).height() == docHeight);

    },
    infiniteMariaScroll: function() {

        var $posts = $("#posts"),
            _maxPages = 15;

        // CHECK IF POSTS IS ON THE PAGE AND CONTAINS AN OBJECT
        if($posts.length && $posts.data("loaded") && ($posts.data("paged") <= _maxPages)) {

            // CHECK IF THE BOTTOM OF THE PAGE
            if (mariasharapova.bottomOfPage()){


                // AJAX NEXT PAGED ITEMS
                var _data = $posts.data(),
                    _nextPage = _data.paged + 1;
                mariasharapova.filterAjax(_data.filter, _nextPage);

                // UPDATE POSTS DATA
                $posts.data("paged", _nextPage);
                $posts.data("filter", _data.filter);

            }

        }

        mariasharapova.swapImages();

    },
    sharrre: function(container) {
        var $container = $(container),
            $twitter = $container.find('.twitter'),
            $facebook = $container.find('.facebook'),
            $instagram = $container.find('.instagram'),
            $googleplus = $container.find('.googleplus');

        $twitter.sharrre({
            share: {
                twitter: true
            },
            url: $twitter.data('url'),
            template: '<a class="box" href="#"><div class="share"><span></span>Tweet</div></a>',
            enableHover: false,
            enableTracking: false,
            buttons: {
                twitter: {
                    via: 'MariaSharapova'
                }
            },
            click: function(api, options) {
                api.simulateClick();
                api.openPopup('twitter');
                ga('send', 'event', 'Twitter Share', 'click', $twitter.data('url'));
            }
        });
        $facebook.sharrre({
            share: {
                facebook: true
            },
            template: '<a class="box" href="#"><div class="share"><span></span>Like</div></a>',
            enableHover: false,
            enableTracking: false,
            click: function(api, options) {
                api.simulateClick();
                api.openPopup('facebook');
                 ga('send', 'event', 'Facebook Share', 'click', $facebook.data('url'));
            }
        });
        $googleplus.sharrre({
            share: {
                googlePlus: true
            },
            template: '<a class="box" href="#"><div class="share"><span></span>Google+</div></a>',
            enableHover: false,
            enableTracking: false,
            urlCurl: "",
            click: function(api, options) {
                api.simulateClick();
                api.openPopup('googlePlus');
                ga('send', 'event', 'Googleplus Share', 'click', $googleplus.data('url'));
            }
        });
    },
    swapImages: function() {
        var viewport = $(window).width();

        // Swap images between full-size and mobile (if present)
        if (viewport < 768) {
            mariasharapova.$img.filter('[data-mobile]').each(function() {
                var $this = $(this),
                    mobileImg = $this.data('mobile');

                if(mobileImg === "") {
                    $this.attr('src', '').closest('.loading').removeClass('loading');
                } else {
                    $this.attr('src', mobileImg).load(function() {
                        $this.closest('.loading').removeClass('loading');
                    });
                }
            });
        } else {
            mariasharapova.$img.filter('[data-full]').each(function() {
                var $this = $(this);
                $this.attr('src', $this.data('full')).load(function() {
                    $this.closest('.loading').removeClass('loading');
                });
            })
        }
    },
    lazyLoadImages: function() {
        mariasharapova.$img.filter('.lazy').unveil(200, function() {
            $(this).load(function() {
                $(this).removeClass('lazy').closest('.hentry').removeClass('loading').find('.throbber').remove();
            });
        });
    },
    backToTop: function() {
        $('#overlap').animate({
            scrollTop: 0
        });
    },
    callbackInfiniteScroll: function() {
        $filterActive = $('#primaryNavigation .filter.active a')

        // Refresh image cache
        mariasharapova.$img = $('img');

        // Lazy load
        mariasharapova.lazyLoadImages();

        // Re-initialize filter
        if ($filterActive.length) {
            mariasharapova.filterPosts($filterActive.attr('href').substr(1));
            $(window).trigger('scroll');
        }

        // Remove loading classes
        mariasharapova.handleLoader();

        //refresh tiles when new one is in
        countrifyLinks();
    },
    initializeTwitterWidget: function() {
        if (typeof twttr.widgets.load === 'function') {
            twttr.widgets.load();
        } else {
            ! function(d, s, id) {
                var js,
                    fjs = d.getElementsByTagName(s)[0],
                    p = /^http:/.test(d.location) ? 'http' : 'https';

                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + "://platform.twitter.com/widgets.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, "script", "twitter-wjs");
        }
    },
    galleryOpenModal: function(e) {
        e.preventDefault();

        var $this = $(this),
            targetURL = $this[0].href, // returns full url instead of possibly a relative url
            title = $this.attr('title'),
            caption = $this.data('excerpt'),
            $modal = $('#modal');

        $this.closest('.gallery-photo').addClass('active');

        if(mariasharapova.$body.hasClass('touch')) {
            $modal.hide();
        } else {
            $modal.fadeIn();
        }

        mariasharapova.galleryLoadImage(targetURL);
        mariasharapova.galleryShareInit($modal, targetURL);
    },
    galleryCloseModal: function(e) {
        var $modal = $('#modal');

        if(mariasharapova.$body.hasClass('touch')) {
            $modal.hide();
        } else {
            $modal.fadeOut();
        }

        $modal.find('.share-open').removeClass('share-open');
    },
    galleryInitControls: function() {
        var $this = $(this),
            $galleryThumbnails = $('#galleryPhotos .gallery-photo'),
            $activeThumbnail = $galleryThumbnails.filter('.active'),
            $nextThumbnail = $activeThumbnail.next(),
            $prevThumbnail = $activeThumbnail.prev(),
            $targetThumbnail;

        if ($this.attr('id') === 'imgNext') {
            if ($nextThumbnail.length) {
                $targetThumbnail = $nextThumbnail;
            } else {
                $targetThumbnail = $galleryThumbnails.first()
            }
        } else if ($this.attr('id') === 'imgPrev') {
            if ($prevThumbnail.length) {
                $targetThumbnail = $prevThumbnail;
            } else {
                $targetThumbnail = $galleryThumbnails.last()
            }
        }
        $targetThumbnail.addClass('active').siblings().removeClass('active');

        mariasharapova.galleryLoadImage($targetThumbnail.find('a')[0].href);
        mariasharapova.galleryShareInit('#modal', $targetThumbnail.find('a')[0].href);
    },
    galleryLoadImage: function(url) {
        $img = $('#modal').find('img');

        if( $img.attr('src') !== url ) {

            $img.fadeOut(200, function() {
                $img.attr('src', url).load(function() {
                    $img.fadeIn();
                });
            });
            $('#modal').find('.share-open').removeClass('share-open');

        }

    },
    galleryShareInit: function(container, url) {
        var $twitter = $('<div class="twitter" data-url="' + url + '" />'),
            $facebook = $('<div class="facebook" data-url="' + url + '" />'),
            $googleplus = $('<div class="googleplus" data-url="' + url + '" />');

        $(container).find('.share-buttons').empty().append($twitter, $facebook, $googleplus);

        mariasharapova.sharrre(container);
    },
    galleryInit: function() {
        var $galleryThumbnails = $('#galleryPhotos .gallery-photo'),
            slide = mariasharapova.getURLParameter('image')

        $('#galleryPhotos').on('click', 'a', mariasharapova.galleryOpenModal);

        $('#closeModal').on('click', mariasharapova.galleryCloseModal);

        $('#imgNext, #imgPrev').on('click', mariasharapova.galleryInitControls);

        if (slide) {
            if ($galleryThumbnails.eq(slide).length) {
                $galleryThumbnails.eq(slide).find('a').trigger('click')
            }
        }

    },
    handleLoader: function() {
        $('.loading').each(function() {
            var $this = $(this);

            if (!$this.find('.entry-thumbnail').length) {
                setTimeout(function() {
                    $this.removeClass('loading');
                }, 300);
            }
        });
    },
    init: function() {

        if( !/filter/.test(window.location.href)) {
            // Load HTML for homepage
           mariasharapova.filterAjax("hentry", 1);
        }

        // Remove loading classes
        mariasharapova.handleLoader();

        // Catch State change
        History.Adapter.bind(window, 'statechange', mariasharapova.handleStateChange);
        $(window).trigger('statechange').trigger('scroll');

        // Main nav toggle
        $('#toggleSiteNavigation').on('click', mariasharapova.toggleMenu);

        // Close menu if anything outside of it is clicked
        $('#header,#content,#footer').on('click', mariasharapova.closeMenu);

        // Share button container toggle
        $('#overlap').on('click', '.toggle-share', mariasharapova.toggleShare);

        // Overlap close button
        $('#closeOverlap').on('click', function() {
            History.pushState(null, null, '/');
        });

        // AJAX links
        $(document).on("click", ".ajax a", mariasharapova.handleClickAjax);

        // Isotope no worky, so now we filter manually
        $('#primaryNavigation').on('click', '.filter a', mariasharapova.handleClickFilter);

        // Enable hash functionality
        // $(window).bind('hashchange', mariasharapova.handleHashChange).trigger('hashchange');

        // Initialize Sponsors slider
        $('#sponsors').flexslider({
            animation: 'slide',
            controlNav: false,
            directionNav: false,
            useCSS: false
        });

        // Load alternate images if present
        $(window).on('resize', mariasharapova.swapImages);
        mariasharapova.swapImages();

        // Lazy load other images
        mariasharapova.lazyLoadImages();

        // Back to top button
        $('#overlap').on('click', '#top', mariasharapova.backToTop);

        // Changing Logo functionality to clear filter on click
        $('#siteLogo').on('click', function(event) {
            event.preventDefault();
            $('#primaryNavigation a').first().trigger('click');
        });
        // Maria Infinite Scroll Event
        $(window).on("scroll", mariasharapova.infiniteMariaScroll);
    }
}

$(document).ready(function() {
    // Load plugins file based on pushState support
    var src = '/wp-content/themes/mariasharapova/assets/js/',
        pluginsURL = (typeof history.pushState === 'undefined') ? 'plugins-bundled.js' : 'plugins.js';

    $.getScript(src + pluginsURL, function() {
        mariasharapova.init();
    });

    $('.fb-like-count').on('click', function() {
      ga('send', 'event', 'Facebook Icon', 'click', 'facebook-icon');
    });
    $('.twitter-follower-count').on('click', function() {
      ga('send', 'event', 'Twitter Icon', 'click', 'twitter-icon');
    });
    $('.link-maria-insta').on('click', function() {
      ga('send', 'event', 'Instagram Icon', 'click', 'instagram-icon');
    });
    $(document).on('click', '.latest-post a' , function() {
        ga('send', 'event', 'Other Latest News', 'click', $(this).attr("href"));
    }).on('blur', '#fieldEmail', function() {
        var $this = $(this);
        if ($(this).is(":invalid")) {
            $(this).parent().addClass('wrong');
        } else {
            $(this).parent().addClass('right');
        }
    });
    $('#posts').on('click', '.read-more.twitter-post' , function() {
        ga('send', 'event', 'Twitter Post', 'click', 'twitter-post');
    });
    $('#posts').on('click','.read-more.facebook-post' , function() {
        ga('send', 'event', 'Facebook Post', 'click', 'facebook-post');
    });
    $('#posts').on('click', '.read-more.instagram-post' , function() {
        ga('send', 'event', 'Instagram Post', 'click', 'instagram-post');
    });
});

$(document).ready(function()  {
    checkCookie();
    $.reverseWhen = function(subordinate /* , ..., subordinateN */ ) {
        var i = 0,
            rejectValues = Array.prototype.slice.call(arguments),
            length = rejectValues.length,

            // the count of uncompleted subordinates
            remaining = length !== 1 || (subordinate && jQuery.isFunction(subordinate.promise)) ? length : 0,

            // the master Deferred. If rejectValues consist of only a single Deferred, just use that.
            deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

            // Update function for both reject and progress values
            updateFunc = function(i, contexts, values) {
                return function(value) {
                    contexts[i] = this;
                    values[i] = arguments.length > 1 ? Array.prototype.slice.call(arguments) : value;
                    if (values === progressValues) {
                        deferred.notifyWith(contexts, values);
                    } else if (!(--remaining)) {
                        deferred.rejectWith(contexts, values);
                    }
                };
            },

            progressValues, progressContexts, rejectContexts;

        // add listeners to Deferred subordinates; treat others as rejected
        if (length > 1) {
            progressValues = new Array(length);
            progressContexts = new Array(length);
            rejectContexts = new Array(length);
            for (; i < length; i++) {
                if (rejectValues[i] && jQuery.isFunction(rejectValues[i].promise)) {
                    rejectValues[i].promise().done(deferred.resolve).fail(updateFunc(i, rejectContexts, rejectValues)).progress(updateFunc(i, progressContexts, progressValues));
                } else {
                    --remaining;
                }
            }
        }

        // if we're not waiting on anything, reject the master
        if (!remaining) {
            deferred.rejectWith(rejectContexts, rejectValues);
        }

        return deferred.promise();
    };

    ///////////
    var d1 = lookupCountryWithFreegeoip();
    var d2 = lookupCountryWithHostipInfo();
    var d3 = lookupLatLongWithBrowserGeolocation().pipe(latLongToCountryWithGeonames);
    var d4 = lookupCountryWithGeoplugin();

    $.reverseWhen(d1, d2, d3, d4).then(function(r,s,t,u,v,w) {
        $('body').append( r.code);

    }, function(x1, x2, x3, x4) {

    });

    $.when(d1, d2, d4).then(function () {

    });
    $.when(d1, d2, d3, d4).then(function () {

    });
});




function CountryLocatorForStorePage(results) {
    var country_code = results.country_code;

    var newzealand_australia = ["AU", "NZ"];
    var europe_middleeast_africa = ["AL", "AD", "AT", "BY", "BE", "BA", "BG", "HR", "CY", "CZ", "DK", "EE", "FO", "FI", "FR", "DE", "GI", "HU", "IS", "IE", "IT", "LV", "LI", "LT", "LU", "MK", "MT", "MD", "MC", "NL", "NO", "PL", "PT", "RO", "RU", "SM", "RS", "SK", "SI", "ES", "SE", "CH", "UA", "GB", "VA", "RS", "IM", "RS", "IM", "ME", "BH", "IQ", "IR", "IL", "JO", "KW", "LB", "OM", "PS", "QA", "SA", "SY", "AE", "YE", "DZ", "AO", "SH", "BJ", "BW", "BF", "BI", "CM", "CV", "CF", "TD", "KM", "CG", "DJ", "EG", "GQ", "ER", "ET", "GA", "GM", "GH", "GN", "CI", "KE", "LS", "LR", "LY", "MG", "MW", "ML", "MR", "MU", "YT", "MA", "NA", "NE", "NG", "ST", "RE", "RW", "ST", "SN", "SZ", "TZ", "TG", "TN", "UG", "CD", "ZM", "TZ", "ZW", "SS", "CD"];
    var field_link = '';

    if ($.inArray(country_code, newzealand_australia) > -1)
        field_link = 'link-for-australia-and-new-zealand';
    else if ($.inArray(country_code, europe_middleeast_africa) > -1)
        field_link = 'link-for-europe-middle-east-and-africa';
    else
        field_link = 'link-for-all';

    $('body').data('country',field_link);

    countrifyLinks();
}

function countrifyLinks() {
    var link_type = $('body').data('country');
    $('.ms_store .entry-thumbnail').each(function () {
        var real_link = $(this).data(link_type);
        if (!real_link)
            real_link = $(this).data('permalink');
        $(this).attr('href', real_link);
    });
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie() {
    var value_of_cookie = getCookie("mariasharapova");
    if (value_of_cookie != "") {
        return;
    } else {
        var d = new Date();
        var number_of_expired_days = 1;
        d.setTime(d.getTime() + (number_of_expired_days*24*60*60*1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = "mariasharapova=yes;" + expires;
    }
}



function lookupLatLongWithBrowserGeolocation() {
    var me = 'browser geolocation',
        dfr = $.Deferred();

    navigator.geolocation.getCurrentPosition(function(position) {

        dfr.resolve({who: me, coords: position.coords});
    });

    return dfr;
}

// supports CORS and JSONP
function latLongToCountryWithGeonames(data) {
    var me = 'geonames',
        dfr = $.ajax({
            url: 'http://ws.geonames.org/countryCode',
            data: {
                lat: data.coords.latitude,
                lng: data.coords.longitude,
                type: 'JSON'
            },
            dataType: $.support.cors ? 'json' : 'jsonp'
        });

    dfr = dfr.pipe(function(results) {
        if (results.countryCode === 'XX') {

            return $.Deferred().reject();
        } else {

            return {who: me, code: results.countryCode};
        }
    });
    return dfr;
}

// supports CORS only
function lookupCountryWithHostipInfo() {
    if ($.support.cors) {  // doesn't support JSONP
        var me = 'hostip.info',
            dfr = $.ajax({
                url: 'http://api.hostip.info/get_json.php',
                dataType: 'json'
            });

        dfr = dfr.pipe(function(results) {
            if (results.country_code === 'XX') {

                return $.Deferred().reject();
            } else {

                return {who: me, code: results.country_code};
            }
        });
        return dfr;
    } else {
        return undefined;
    }
}

// supports JSONP only
function lookupCountryWithGeoplugin() {
    var me = 'geoplugin',
        dfr = $.ajax({
            url: 'http://www.geoplugin.net/json.gp?',
            dataType: 'jsonp', // doesn't support CORS
            data: {
                'native': 1
            },
            jsonp: 'jsoncallback'
        });

    dfr = dfr.pipe(function(results) {

        return {who: me, code: results.geoplugin_countryCode};
    });
    return dfr;
}

// supports CORS and JSONP
function lookupCountryWithFreegeoip() {
    var me = 'freegeoip',
        dfr = $.ajax({
            url: 'http://freegeoip.net/json/',
            dataType: $.support.cors ? 'json' : 'jsonp'
        });

    dfr = dfr.pipe(function(results) {
        CountryLocatorForStorePage(results);
        return {who: me, code: results.country_code};
    });
    return dfr;
}


$(function(){

    $('#overlapContent').on('click', '[data-postid]', function(event) {
        event.preventDefault();
        $('.selected-store').removeClass('selected-store');
        $(this).addClass('selected-store');
        var storeItemId = $(this).data("postid");
        var _ajaxUrlStore = "/store-items/?id=" + storeItemId;
        //START AJAX REQUEST
        $.ajax({
            type: "GET",
            url: _ajaxUrlStore,
            dataType : 'html'
        }).done(function(response) {
            $('.close-store').addClass('store-button');
            $('.store-list').remove();
            $('#contentArticle').append(response);
            // $('#storeList').addClass('active').siblings('.partner-post').hide();
            $('#closeStore').on('click', function(){
                $('#storeList').removeClass('active');
                $('#closeStore').removeClass('store-button');
                $('#storeList').siblings('.partner-post').show();
            })

        });

        countrifyLinks();
    });



});
