requirejs.config({
  baseUrl: "/assets/js/",
  paths: {
    jquery: "//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min",
    "jquery.migrate": "//code.jquery.com/jquery-migrate-1.2.1"
  },
  shim: {
    "jquery.migrate": {
      deps: ["jquery"]
    },
    "jquery.mobile.events": {
      deps: ["jquery"]
    }
  }
});

require(["modules/init"]);
