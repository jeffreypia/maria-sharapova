define([], function() {
  var requireIf;
  requireIf = function(moduleArray, condition, elseFunction) {
    if (typeof condition === "undefined" || condition) {
      return require(moduleArray);
    } else {
      if (typeof elseFunction !== "undefined") {
        return elseFunction();
      }
    }
  };
  return requireIf;
});
