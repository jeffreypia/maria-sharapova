define(["jquery", "vendor/jquery.royalslider.min"], function($) {
  var accordion;
  accordion = {
    config: {
      defaultOpenIndex: 0,
      accordionSelector: ".accordion-title",
      iconSelector: ".accordion-state",
      EXPAND_CLASS: "active",
      ICON_OPEN_CLASS: "icon-plus",
      ICON_CLOSE_CLASS: "icon-minus"
    },
    init: function() {
      var _self;
      _self = this;
      return _self.events();
    },
    events: function() {
      var $defaultOpenSelector, _clickSelector, _self;
      _self = this;
      _clickSelector = _self.config.accordionSelector + " a";
      $defaultOpenSelector = $(_self.config.accordionSelector).eq(_self.config.defaultOpenIndex);
      $(document).off().on("click", _clickSelector);
      return $(document).on("click", _clickSelector, function(e) {
        var $currentSelector, $this, _expand, _isExpanded, _selector;
        e.preventDefault();
        $this = $(this);
        _selector = _self.config.accordionSelector;
        _expand = _self.config.EXPAND_CLASS;
        $currentSelector = $this.parent();
        _isExpanded = $currentSelector.hasClass(_expand);
        if (_isExpanded) {
          return _self.closeSection($currentSelector);
        } else {
          return _self.openSection($currentSelector);
        }
      });
    },
    openSection: function($selector) {
      var _close, _config, _expand, _icon, _open, _remove, _self;
      _self = this;
      _config = _self.config;
      _expand = _config.EXPAND_CLASS;
      _icon = _config.iconSelector;
      _close = _config.ICON_CLOSE_CLASS;
      _open = _config.ICON_OPEN_CLASS;
      _remove = _open + " " + _close;
      $selector.addClass(_expand);
      $selector.next().addClass(_expand);
      return $selector.find(_icon).removeClass(_remove).addClass(_close);
    },
    closeSection: function($selector) {
      var _close, _config, _expand, _icon, _open, _remove, _self;
      _self = this;
      _config = _self.config;
      _expand = _config.EXPAND_CLASS;
      _icon = _config.iconSelector;
      _close = _config.ICON_CLOSE_CLASS;
      _open = _config.ICON_OPEN_CLASS;
      _remove = _open + " " + _close;
      $selector.removeClass(_expand);
      $selector.next().removeClass(_expand);
      return $selector.find(_icon).removeClass(_remove).addClass(_open);
    }
  };
  return accordion.init();
});
