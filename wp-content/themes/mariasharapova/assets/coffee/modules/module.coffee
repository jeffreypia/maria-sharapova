# EXAMPLE
define ["jquery", "vendor/jquery.royalslider.min"], ($) ->
  accordion =
    config:
      defaultOpenIndex: 0 # which accordion section do you want open first on init
      accordionSelector: ".accordion-title" # accordion selector used for click events
      iconSelector: ".accordion-state" # icon selector used for switching icons
      EXPAND_CLASS: "active" # accordion expan class constant
      ICON_OPEN_CLASS: "icon-plus" # icon open constant
      ICON_CLOSE_CLASS: "icon-minus" # icon close constant

    init: ->
      _self = this
      _self.events()

    events: ->
      _self = this
      _clickSelector = _self.config.accordionSelector + " a"
      $defaultOpenSelector = $(_self.config.accordionSelector).eq(_self.config.defaultOpenIndex)
      
      #  open up an accordion panel	
      $(document).off().on "click", _clickSelector
      $(document).on "click", _clickSelector, (e) ->
        e.preventDefault()
        $this = $(this)
        _selector = _self.config.accordionSelector
        _expand = _self.config.EXPAND_CLASS
        $currentSelector = $this.parent()
        _isExpanded = $currentSelector.hasClass(_expand)
        if _isExpanded
          _self.closeSection $currentSelector
        else
          _self.openSection $currentSelector


    openSection: ($selector) ->
      _self = this
      _config = _self.config
      _expand = _config.EXPAND_CLASS
      _icon = _config.iconSelector
      _close = _config.ICON_CLOSE_CLASS
      _open = _config.ICON_OPEN_CLASS
      _remove = _open + " " + _close
      
      # expand title
      $selector.addClass _expand
      
      # expand content
      $selector.next().addClass _expand
      
      # update icon
      $selector.find(_icon).removeClass(_remove).addClass _close

    closeSection: ($selector) ->
      _self = this
      _config = _self.config
      _expand = _config.EXPAND_CLASS
      _icon = _config.iconSelector
      _close = _config.ICON_CLOSE_CLASS
      _open = _config.ICON_OPEN_CLASS
      _remove = _open + " " + _close
      
      # expand title
      $selector.removeClass _expand
      
      # expand content
      $selector.next().removeClass _expand
      
      # update icon
      $selector.find(_icon).removeClass(_remove).addClass _open

  
  # run
  accordion.init()

# end define