define [], ->
  requireIf = (moduleArray, condition, elseFunction) ->
    if typeof condition is "undefined" or condition
      require moduleArray
    else elseFunction()  unless typeof elseFunction is "undefined"

  requireIf