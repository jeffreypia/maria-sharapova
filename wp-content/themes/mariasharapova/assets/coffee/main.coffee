requirejs.config
  baseUrl: "/assets/js/"
  paths:
    jquery: "//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min" # path to the latest jquery version
    "jquery.migrate": "//code.jquery.com/jquery-migrate-1.2.1" # path to the latest jquery migrate version - this is to fix compatibility issues with older browsers and plugins

  shim:
    "jquery.migrate":
      deps: ["jquery"]

    "jquery.mobile.events":
      deps: ["jquery"]


# require these files
require ["modules/init"]