<?php
/**
 * The Header for our theme
 */
?><!DOCTYPE html>
<!--[if lte IE 7]>
<html class="no-js ie ie7 ltie8 ltie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="no-js ie ie8 ltie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html class="no-js ie ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9) ]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<title data-orig="<?php wp_title() ?>"><?php wp_title(); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/vendor/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

	<meta property="og:image" content="http://www.mariasharapova.com/logo.png"/>

	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/royalslider.css">
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/royalslider_default.css">
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/mariasharapova.css" />
	<!--
	<link rel="stylesheet" href="http://localhost:8080/assets/css/mariasharapova.css" />
	-->
<?php
	// All pages should re-direct to home page and have content AJAX'd in
	if(!is_home()) :
?>
		<script type="text/javascript">
			window.location.href = window.location.protocol + "//" + window.location.host + "/#" + window.location.pathname + window.location.search;
		</script>

<?php endif ?>

	<!-- Favicons -->
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196">
	<meta name="msapplication-TileColor" content="#2d89ef">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
</head>

<body <?php body_class(); ?> data-country="<?php $field_link;?>">
<div id="fb-root"></div>

<header id="header" class="site-header" role="banner">
	<?php if( is_home() ) : ?>
	<h1 class="site-logo"><a id="siteLogo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Sharapova</a></h1>
	<?php else : ?>
	<div class="site-logo"><a id="siteLogo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Sharapova</a></div>
	<?php endif; ?>

	<button id="toggleSiteNavigation" class="toggle-site-navigation"><span>Menu</span></button>
</header><!-- #header -->

<?php
	wp_nav_menu(
		array(
			'container'			=> 'nav',
			'container_class'	=> 'site-navigation',
			'container_id'		=> 'primaryNavigation',
			'theme_location' 	=> 'primary',
		)
	);
?>

<section id="content" class="content">
