<?php 
	// Template for Sponsor Posts
		
		$postLink = lm_get('ms_ad_link');
		$postBg = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

		if( $postBg ) :
?>
		<div <?php post_class('loading'); ?> id="<?php echo $post->post_name; ?>">
			<?php if( $postLink ) : ?>
			<a class="entry-thumbnail" target="_blank" href="<?php echo $postLink; ?>">
				<noscript><img src="<?php echo $postBg; ?>" alt=""></noscript>
				<img src="" data-src="<?php echo $postBg; ?>" alt="" class="lazy">
			</a>
			<?php else : ?>
			<div class="entry-thumbnail">
				<img src="" data-src="<?php echo $postBg; ?>" alt="<?php the_title(); ?>" class="lazy">
			</div>
			<?php endif; ?>

			<?php edit_post_link('Edit post'); ?>

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
		<?php endif; ?>