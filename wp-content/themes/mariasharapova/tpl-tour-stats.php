<?php
/**
 * Template Name: Tour Stats
 */

get_header(); 

	while ( have_posts() ) : the_post();
?>
	<section id="contentArticle" class="content-article tour-stats" role="main">

		<?php if( has_post_thumbnail() ) : ?>
		<figure class="banner">
			<img src="" data-src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" alt="" class="lazy">
		</figure>
		<?php endif; ?>

		<article class="entry-content">
			<header class="article-header">
				<h1 class="article-title"><?php the_title(); ?></h1>
				<?php edit_post_link('Edit post'); ?>
			</header>
			
			<?php echo get_the_content(); ?>

			<footer class="article-footer">
				<?php get_template_part('partial', 'share'); ?>
			</footer>

		</article>

	</section><!-- #contentMain -->

	<?php endwhile; ?>

<?php get_footer();