<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>

	<section id="contentArticle" class="content-article" role="main">

		<article class="entry-content">
			<header class="article-header">
				<h1 class="article-title">404 Error</h1>
				<?php edit_post_link('Edit post'); ?>
			</header>
			
			<p>Page Not Found</p>

			<footer>
				<div class="share-buttons">
					<div class="twitter" id="twitter" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>">Twitter</div>
					<div class="facebook" id="facebook" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>">Facebook</div>
					<div class="googleplus" id="googleplus" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>">GooglePlus</div>
				</div><!-- .share-buttons -->
			</footer>
		</article>

	</section><!-- #contentArticle -->

	<section class="sidebar">
		<?php get_template_part( 'loop', 'latest-posts' ); ?>
	</section><!-- .sidebar -->

<?php get_footer(); ?>