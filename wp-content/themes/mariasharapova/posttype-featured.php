<?php 
	// Template for Featured Posts
		
	// Get Featured CTA
	$args = array(
		'posts_per_page'	=> 5,
		'post_type'			=> array( 'ms_featured' ),
	);

	$featured = get_posts( $args );

?>
	<div class="hentry ms_featured">
 	<div id="content-slider-1" class="royalSlider contentSlider rsDefault">
    
<?php 
	 $i = 0;
	foreach( $featured as $post ) : setup_postdata($post);
		$i++;
		$title = get_the_title();
		$classes = 'no-filter loading';
		$classes .= $title ? '' : ' ms_ad';
		$postLink = lm_get('ms_featured_link');
		$postBg = lm_get_img_src('ms_featured_desktop_thumbnail', 'full');
		$postBgMobile = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>
		<div>
<?php 
			// If title is present, display post normally
			// If no title, only show bg image wrapped in a like (like an ad)
			if( $title ) :

				// Only output thumbnail markup if images exist
				if( $postBg || $postBgMobile ) :
?>
				
				<div class="entry-thumbnail">
					<noscript><img src="<?php echo $postBgMobile; ?>" alt=""></noscript>
					<img src="" data-full="<?php echo $postBg ?>" data-mobile="<?php echo $postBgMobile ?>" alt="">
				</div>
				<?php endif; ?>

				<div class="content-wrapper">
					
					<div class="entry-date">
						<div class="entry-month"><?php echo get_the_date('M'); ?></div>
						<div class="entry-day"><?php echo get_the_date('j'); ?></div>
					</div><!-- .entry-date -->

					<h3 class="entry-category">Featured</h3>

					<h2 class="entry-title"><?php the_title(); ?></h2>

					<?php edit_post_link('Edit post'); ?>

					<?php if ( $postLink ) : ?>
					<div class="button-container">
						<a target="_blank" href="<?php echo $postLink; ?>" class="read-more" data-slider="<?php echo $i?>">Read More</a>
					</div>
					<?php endif; ?>
				
				</div><!-- .content-wrapper -->

			<?php else : ?>

				<a class="entry-thumbnail" target="_blank" href="<?php echo $postLink; ?>">
					<noscript><img src="<?php echo $postBg; ?>" alt=""></noscript>
					<img src="" data-full="<?php echo $postBg ?>" data-mobile="<?php echo $postBgMobile ?>" alt="">
				</a>

			<?php endif; ?>

			<?php get_template_part( 'partial', 'throbber' ); ?>

		</div>
<?php
	endforeach;
?>
</div>
</div>

