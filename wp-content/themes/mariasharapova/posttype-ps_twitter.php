<?php 
	// Twitter post template

	$postBg = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	$twitterID = lm_get('twitter_sync_id');
	// $twitterHandle = get_option( 'twitter_sync_option' );
	// $twitterLinkID
?>
		<div <?php post_class('social loading'); ?> id="<?php echo $post->post_name; ?>">

			<?php if( $postBg ) : ?>
			<div class="entry-thumbnail">
				<noscript><img src="<?php echo $postBg; ?>" alt=""></noscript>
				<img src="" data-src="<?php echo $postBg; ?>" alt="" class="lazy">
			</div>
			<?php endif; ?>
			
			<div class="content-wrapper">
				<div class="entry-date">
					<div class="entry-month"><?php echo get_the_date('M'); ?></div>
					<div class="entry-day"><?php echo get_the_date('j'); ?></div>
				</div>

				<h3 class="entry-category">
					MariaSharapova <br>
					<span><?php echo lm_tweet_text_formatter( '@MariaSharapova' ); ?></span>
				</h3>

				<?php edit_post_link('Edit post'); ?>

				<div class="entry-content"><?php echo lm_tweet_text_formatter( get_the_content() ); ?></div>

				<div class="social-icons">
					<a target="_blank" class="reply" href="https://twitter.com/intent/tweet?in_reply_to=<?php echo $twitterID; ?>">Reply</a>
					<a target="_blank" class="retweet" href="https://twitter.com/intent/retweet?tweet_id=<?php echo $twitterID; ?>">Retweet</a>
					<a target="_blank" class="favorite" href="https://twitter.com/intent/favorite?tweet_id=<?php echo $twitterID; ?>">Favorite</a>
				</div>

				<div class="button-container">
					<a target="_blank" href="https://twitter.com/statuses/<?php echo $twitterID; ?>" class="read-more twitter-post">View on Twitter</a>
				</div>

			</div><!-- .content-wrapper -->

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
