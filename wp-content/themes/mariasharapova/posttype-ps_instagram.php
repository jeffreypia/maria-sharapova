<?php 
	// Instagram post template
	$postBg = lm_get('instagram_sync_image_url');
	$instagramLink = lm_get('instagram_sync_link');
	// $InstagramHandle = get_option( 'twitter_sync_option' );
	// $InstagramLinkID
?>
		<div <?php post_class('social loading'); ?> id="<?php echo $post->post_name; ?>">

			<?php if( $postBg ) : ?>
			<div class="entry-thumbnail">
				<noscript><img src="<?php echo $postBg; ?>" alt=""></noscript>
				<img src="" data-src="<?php echo $postBg; ?>" alt="" class="lazy">
			</div>
			<?php endif; ?>
			
			<div class="content-wrapper">
				<div class="entry-date">
					<div class="entry-month"><?php echo get_the_date('M'); ?></div>
					<div class="entry-day"><?php echo get_the_date('j'); ?></div>
				</div>

				<h3 class="entry-category">
					MariaSharapova <br>
					<span><?php echo lm_instagram_text_formatter( '@MariaSharapova' ); ?></span>
				</h3>

				<?php edit_post_link('Edit post'); ?>

				<div class="entry-content"><?php echo lm_linkify( get_the_content() ); ?></div>

				<div class="button-container">
					<a target="_blank" href=<?php echo $instagramLink; ?> class="read-more instagram-post">View on Instagram</a>
				</div>

			</div><!-- .content-wrapper -->

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
