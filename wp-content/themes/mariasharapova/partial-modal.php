<figure id="modal" class="modal">
	<button id="closeModal" class="close"><span>Close</span></button>

	<div class="modal-content">

		<?php get_template_part( 'partial', 'gallery-full' ); ?>

		<footer class="modal-footer">
			<?php get_template_part( 'partial', 'share' ); ?>
		</footer>
	</div>

	<?php get_template_part( 'partial', 'throbber' ); ?>
</figure>
