<?php 
	// Facebook post template

	$postBg = lm_get('fb_sync_picture');
	$postBg = str_replace('_s', '_n', $postBg); // pull large image instead of small
	$postLink = lm_get('fb_sync_link');
?>
		<div <?php post_class('social loading'); ?> id="<?php echo $post->post_name; ?>">

			<?php if( $postBg ) : ?>
			<div class="entry-thumbnail">
				<noscript><img src="<?php echo $postBg; ?>" alt=""></noscript>
				<img src="" data-src="<?php echo $postBg; ?>" alt="" class="lazy">
			</div>
			<?php endif; ?>
			
			<div class="content-wrapper">
				<div class="entry-date">
					<div class="entry-month"><?php echo get_the_date('M'); ?></div>
					<div class="entry-day"><?php echo get_the_date('j'); ?></div>
				</div>

				<h3 class="entry-category">
					MariaSharapova <br>
					<span><a target="_blank" href="http://facebook.com/sharapova">via Facebook</a></span>
				</h3>

				<?php edit_post_link('Edit post'); ?>

				<div class="entry-content"><?php echo lm_linkify( get_the_content() ); ?></div>

				<div class="button-container">
					<a target="_blank" href="<?php echo $postLink; ?>" class="read-more facebook-post">View on Facebook</a>
				</div>

			</div><!-- .content-wrapper -->

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
