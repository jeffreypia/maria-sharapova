<?php
	// Loop of latest blog posts
	$args = array(
		'posts_per_page'	=> 6,
		'post_type'			=> array( 'post' ),
	);
	$latestPosts = get_posts( $args );
	$current_postid = $post->ID;
?>
		<h3 class="sidebar-title">Other Latest News</h3>
		<ul class="latest-posts">
		
<?php
			if( $latestPosts ) :
		
			foreach( $latestPosts as $post ) : setup_postdata($post); 

				if ($post->ID != $current_postid) :
		?>
		
				<li <?php post_class('latest-post ajax'); ?>>
					<div class="content-wrapper">
						<a href="<?php echo the_permalink(); ?>" title="<?php the_title_attribute( array( 'before' => 'Permalink to: ', 'after' => '' ) ); ?>" class="entry-title">
							<?php the_title(); ?>
						</a>
						<div class="entry-excerpt"><?php the_excerpt(); ?></div>

						<?php edit_post_link('Edit post'); ?>
					</div>

					<div class="entry-thumbnail">

						<a href="<?php echo the_permalink(); ?>" title="<?php the_title_attribute( array( 'before' => 'Permalink to: ', 'after' => '' ) ); ?>">
							<?php echo has_post_thumbnail() ? get_the_post_thumbnail($post->ID, 'thumbnail') :  '<img src="http://placehold.it/225x225.jpg" alt="">'; ?>
						</a>

					</div>
		
<?php
				endif;
			endforeach;

		else :
?>
			<li>No recent posts</li>
		<?php endif; ?>

		</ul>
