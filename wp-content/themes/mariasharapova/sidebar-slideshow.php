<?php GLOBAL $slideshow; ?>
<section class="sidebar slideshow">
	<ul class="slides">

	<?php foreach($slideshow as $slideshowImage) : ?>
		<li class="slide">
			<img src="<?php echo wp_get_attachment_url( $slideshowImage[image] ); ?>" alt="">
		</li>
	<?php endforeach; ?>

	</ul>
</section><!-- .sidebar -->
