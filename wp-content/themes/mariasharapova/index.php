<?php
/**
 * The home page template
 *
 */

get_header();
$skipID; // Currently used get ID of most recent featured post and skip it in the main loop
?>

<main id="contentMain" class="content-main" role="main">
	<div id="posts" class="posts">
	</div><!-- #posts -->
</main><!-- #contentMain -->

<nav id="nav-below" class="nav-below">
	<?php previous_posts_link('&laquo; Newer') ?>
	<?php next_posts_link('Older &raquo;') ?>
</nav>

<?php get_template_part( 'partial', 'overlap' ); ?>

<?php get_footer(); ?>
