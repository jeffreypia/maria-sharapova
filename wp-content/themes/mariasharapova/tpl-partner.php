<?php
/**
 *  Template Name: partner Page
 */

get_header(); 


$args = array(
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'post_type' => 'ms_partner',
	'post_status' => 'publish',
);

$posts = get_posts( $args );

	while ( have_posts() ) : the_post();

	$partnerIDs = rpt_get_object_relation($posts[0]->ID, 'ms_partner');
	if ( $partnerIDs ) {
		$args = array(
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'post_type' => 'ms_partner_item',
			'post_status' => 'publish',
			'post__in' => $partnerIDs,
		);

		$partners = get_posts( $args );
	}
	
?>

	<section id="contentArticle" class="content-article partner" role="main">

				<article class="partner-post has-partner dynamic-partner posts">
					<?php
						foreach ( $posts as $post ) : setup_postdata( $post );
							get_template_part( 'posttype', 'ms_partner' );
						endforeach;
						?>
				</article>


	</section><!-- #contentMain -->

	<?php endwhile; ?>

<?php get_footer();?>