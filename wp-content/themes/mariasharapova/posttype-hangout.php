<?php
	// Template for Google Hangout

	// Get Google Hangout post
	$args = array(
		'posts_per_page'	=> 1,
		'post_type'			=> array( 'ms_hangout' ),
	);
	$hangout = get_posts( $args );

	foreach( $hangout as $post ) : setup_postdata($post);
		$postLink = lm_get('ms_hangout_link');
		$postDescription = lm_get('ms_hangout_description');
		if( $postLink ) :
?>

		<div <?php post_class('no-filter loading'); ?> id="<?php echo $post->post_name; ?>">

			<div class="content-wrapper">
				<div class="entry-date">
					<div class="entry-month"><?php echo get_the_date('M'); ?></div>
					<div class="entry-day"><?php echo get_the_date('j'); ?></div>
				</div><!-- .entry-date -->

				<h3 class="entry-category">Maria is Chatting Now&hellip;</h3>

				<h2 class="entry-title"><?php echo $postDescription ? $postDescription : get_the_title() ?></h2>

				<?php edit_post_link('Edit post'); ?>

				<div class="button-container">
					<a target="_blank" href="<?php echo $postLink; ?>" class="read-more">Join Hangout Now</a>
				</div>

			</div><!-- .content-wrapper -->

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
<?php
		endif;
	endforeach;
?>