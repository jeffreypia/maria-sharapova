<?php 
	// General post template

	$postBg = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	$categories = get_the_category(); 
	$category = $categories[0]->name;
	$postFormat = get_post_format();
	$postFormat = trim($postFormat);
	$buttonText = "Read More";

	if( $category === "Uncategorized" ) :
		
		if( $postFormat === "gallery" ) :
			$category =  "Photos";
		elseif( $postFormat === "video" ) :
			$category =  "Video";
		else :
			$category = "Post";
		endif;

	endif;

	if ($postFormat === "gallery") {
		$buttonText = "View Gallery";
	}

	if ($postFormat === "video") {
		$buttonText = "Watch Now";
	}

?>
		<div <?php post_class('loading'); ?>>
			<?php if( $postBg ) : ?>
			<div class="entry-thumbnail">
				<noscript><img src="<?php echo $postBg; ?>" alt=""></noscript>
				<img src="" data-src="<?php echo $postBg; ?>" alt="" class="lazy">
			</div>
			<?php endif; ?>
			
			<div class="content-wrapper">
				<div class="entry-date">
					<div class="entry-month"><?php echo get_the_date('M'); ?></div>
					<div class="entry-day"><?php echo get_the_date('j'); ?></div>
				</div>

				<h3 class="entry-category"><?php echo $category; ?></h3>

				<h2 class="entry-title"><?php the_title(); ?></h2>

				<?php edit_post_link('Edit post'); ?>

				<div class="button-container ajax">
					<a href="<?php the_permalink(); ?>" class="read-more"><?php echo $buttonText; ?></a>
				</div>

			</div><!-- .content-wrapper -->

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
