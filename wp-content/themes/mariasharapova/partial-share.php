<button class="toggle-share">Share</button>

<div class="share-buttons">
	<div class="twitter" id="twitter" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>">Twitter</div>
	<div class="facebook" id="facebook" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>">Facebook</div>
	<div class="googleplus" id="googleplus" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>">GooglePlus</div>
</div><!-- .share-buttons -->
