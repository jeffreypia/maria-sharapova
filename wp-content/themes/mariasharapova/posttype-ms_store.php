
<?php 

	$link_for_australia_new_zealand = get_field('link_for_australia_new_zealand');

	$link_for_europe_middle_east_africa = get_field('link_for_europe_middle_east_africa');

	$link_for_all_other_countries = get_field('link_for_all_other_countries');


		$postBg = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

		if( $postBg ) :
?>
		<div <?php post_class($class); ?> id="<?php echo $post->post_name; ?>">
				<?php if( get_field('store_items') ) { ?>
					<a class="entry-thumbnail"  target="_blank" data-link-for-australia-and-new-zealand="<?php echo $link_for_australia_new_zealand; ?>" data-link-for-europe-middle-east-and-africa="<?php echo $link_for_europe_middle_east_africa; ?>" data-link-for-all="<?php echo $link_for_all_other_countries; ?>" data-permalink="<?php echo get_permalink(); ?>" data-postid="<?php echo $post->ID;?>" href="#">
				<?php } else { ?>
					<a class="entry-thumbnail"  target="_blank" data-link-for-australia-and-new-zealand="<?php echo $link_for_australia_new_zealand; ?>" data-link-for-europe-middle-east-and-africa="<?php echo $link_for_europe_middle_east_africa; ?>" data-link-for-all="<?php echo $link_for_all_other_countries; ?>" data-permalink="<?php echo get_permalink(); ?>" href="#">
				<?php } ?>
					<noscript><img src="<?php echo $postBg; ?>" alt=""></noscript>
					<img src="" data-src="<?php echo $postBg; ?>" alt="" class="lazy">
					</a>


			

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
		<?php endif; ?>

