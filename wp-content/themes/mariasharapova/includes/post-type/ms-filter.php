<?php

add_action( 'init', 'register_cpt_filter' );

function register_cpt_filter() {

    $labels = array( 
        'name' => _x( 'Filter', 'ms_filter' ),
        'singular_name' => _x( 'Filter', 'ms_filter' ),
        'add_new' => _x( 'Add New Filter', 'ms_filter' ),
        'all_items' => _x( 'Ads', 'ms_filter' ),
        'add_new_item' => _x( 'Add New Filter', 'ms_filter' ),
        'edit_item' => _x( 'Edit Filter', 'ms_filter' ),
        'new_item' => _x( 'New Filter', 'ms_filter' ),
        'view_item' => _x( 'View Filter', 'ms_filter' ),
        'search_items' => _x( 'Search Filter', 'ms_filter' ),
        'not_found' => _x( 'No Filter found', 'ms_filter' ),
        'not_found_in_trash' => _x( 'No Filter found in Trash', 'ms_filter' ),
        'parent_item_colon' => _x( 'Parent Filter:', 'ms_filter' ),
        'menu_name' => _x( 'Ad', 'ms_filter' )
    );

    $supports = array(
        'title',
        'thumbnail',
        'page-attributes',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => false,
        'exclude_from_search' => true,
        'menu_position' => 8,
        'menu_icon' => 'dashicons-admin-links',
    );

    register_post_type( 'ms_filter', $args );
}