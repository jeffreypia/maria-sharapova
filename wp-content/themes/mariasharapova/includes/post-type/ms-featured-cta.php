<?php

add_action( 'init', 'register_cpt_ms_featured' );

function register_cpt_ms_featured() {

    $labels = array( 
        'name' => _x( 'Featured CTAs', 'ms_featured' ),
        'singular_name' => _x( 'Featured CTA', 'ms_featured' ),
        'add_new' => _x( 'Add New Featured CTA', 'ms_featured' ),
        'all_items' => _x( 'Featured CTAs', 'ms_featured' ),
        'add_new_item' => _x( 'Add New Featured CTA', 'ms_featured' ),
        'edit_item' => _x( 'Edit Featured CTA', 'ms_featured' ),
        'new_item' => _x( 'New Featured CTA', 'ms_featured' ),
        'view_item' => _x( 'View Featured CTA', 'ms_featured' ),
        'search_items' => _x( 'Search Featured CTAs', 'ms_featured' ),
        'not_found' => _x( 'No Featured CTAs found', 'ms_featured' ),
        'not_found_in_trash' => _x( 'No Featured CTAs found in Trash', 'ms_featured' ),
        'parent_item_colon' => _x( 'Parent Featured CTA:', 'ms_featured' ),
        'menu_name' => _x( 'Featured CTA', 'ms_featured' ),
    );

    $supports = array(
        'title',
        'thumbnail',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 2,
        'menu_icon' => 'dashicons-star-filled',
    );

    register_post_type( 'ms_featured', $args );
}
