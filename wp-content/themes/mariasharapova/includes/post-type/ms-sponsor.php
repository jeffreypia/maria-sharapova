<?php

add_action( 'init', 'register_cpt_sponsor' );

function register_cpt_sponsor() {

    $labels = array( 
        'name' => _x( 'Sponsor', 'ms_sponsor' ),
        'singular_name' => _x( 'Sponsor', 'ms_sponsor' ),
        'add_new' => _x( 'Add New Sponsor', 'ms_sponsor' ),
        'all_items' => _x( 'Sponsors', 'ms_sponsor' ),
        'add_new_item' => _x( 'Add New Sponsor', 'ms_sponsor' ),
        'edit_item' => _x( 'Edit Sponsor', 'ms_sponsor' ),
        'new_item' => _x( 'New Sponsor', 'ms_sponsor' ),
        'view_item' => _x( 'View Sponsor', 'ms_sponsor' ),
        'search_items' => _x( 'Search Sponsors', 'ms_sponsor' ),
        'not_found' => _x( 'No Sponsors found', 'ms_sponsor' ),
        'not_found_in_trash' => _x( 'No Sponsors found in Trash', 'ms_sponsor' ),
        'parent_item_colon' => _x( 'Parent Sponsor:', 'ms_sponsor' ),
        'menu_name' => _x( 'Sponsor', 'ms_sponsor' )
    );

    $supports = array(
        'title',
        'thumbnail',
        'page-attributes',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 8,
        'menu_icon' => 'dashicons-groups',
    );

    register_post_type( 'ms_sponsor', $args );
}