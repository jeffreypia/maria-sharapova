<?php

add_action( 'init', 'register_cpt_store_item' );

function register_cpt_store_item() {

    $labels = array( 
        'name' => _x( 'Store Item', 'ms_store_item' ),
        'singular_name' => _x( 'Store Item', 'ms_store_item' ),
        'add_new' => _x( 'Add New Store Item', 'ms_store_item' ),
        'all_items' => _x( 'Store Items', 'ms_store_item' ),
        'add_new_item' => _x( 'Add New Store Item', 'ms_store_item' ),
        'edit_item' => _x( 'Edit Store Item', 'ms_store_item' ),
        'new_item' => _x( 'New Store Item', 'ms_store_item' ),
        'view_item' => _x( 'View Store Item', 'ms_store_item' ),
        'search_items' => _x( 'Search Store Items', 'ms_store_item' ),
        'not_found' => _x( 'No Store Items found', 'ms_store_item' ),
        'not_found_in_trash' => _x( 'No Store Items found in Trash', 'ms_store_item' ),
        'parent_item_colon' => _x( 'Parent Store Item:', 'ms_store_item' ),
        'menu_name' => _x( 'Store Item', 'ms_store_item' )
    );

    $supports = array(
        'title',
        'thumbnail',
        'page-attributes',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 8,
        'menu_icon' => 'dashicons-cart',
    );

    register_post_type( 'ms_store_item', $args );
}