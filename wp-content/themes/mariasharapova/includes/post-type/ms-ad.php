<?php

add_action( 'init', 'register_cpt_ad' );

function register_cpt_ad() {

    $labels = array( 
        'name' => _x( 'Ad', 'ms_ad' ),
        'singular_name' => _x( 'Ad', 'ms_ad' ),
        'add_new' => _x( 'Add New Ad', 'ms_ad' ),
        'all_items' => _x( 'Ads', 'ms_ad' ),
        'add_new_item' => _x( 'Add New Ad', 'ms_ad' ),
        'edit_item' => _x( 'Edit Ad', 'ms_ad' ),
        'new_item' => _x( 'New Ad', 'ms_ad' ),
        'view_item' => _x( 'View Ad', 'ms_ad' ),
        'search_items' => _x( 'Search Ads', 'ms_ad' ),
        'not_found' => _x( 'No Ads found', 'ms_ad' ),
        'not_found_in_trash' => _x( 'No Ads found in Trash', 'ms_ad' ),
        'parent_item_colon' => _x( 'Parent Ad:', 'ms_ad' ),
        'menu_name' => _x( 'Ad', 'ms_ad' )
    );

    $supports = array(
        'title',
        'thumbnail',
        'page-attributes',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 8,
        'menu_icon' => 'dashicons-admin-links',
    );

    register_post_type( 'ms_ad', $args );
}