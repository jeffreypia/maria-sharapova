<?php

add_action( 'init', 'register_cpt_ms_schedule' );

function register_cpt_ms_schedule() {

    $labels = array( 
        'name' => _x( 'Schedule', 'ms_schedule' ),
        'singular_name' => _x( 'Schedule', 'ms_schedule' ),
        'add_new' => _x( 'Add New Schedule', 'ms_schedule' ),
        'all_items' => _x( 'Schedules', 'ms_schedule' ),
        'add_new_item' => _x( 'Add New Schedule', 'ms_schedule' ),
        'edit_item' => _x( 'Edit Schedule', 'ms_schedule' ),
        'new_item' => _x( 'New Schedule', 'ms_schedule' ),
        'view_item' => _x( 'View Schedule', 'ms_schedule' ),
        'search_items' => _x( 'Search Schedules', 'ms_schedule' ),
        'not_found' => _x( 'No Schedules found', 'ms_schedule' ),
        'not_found_in_trash' => _x( 'No Schedules found in Trash', 'ms_schedule' ),
        'parent_item_colon' => _x( 'Parent Schedule:', 'ms_schedule' ),
        'menu_name' => _x( 'Schedule', 'ms_schedule' )
    );

    $supports = array(
        'title',
        'thumbnail',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 7,
        'menu_icon' => 'dashicons-calendar',
    );

    register_post_type( 'ms_schedule', $args );
}