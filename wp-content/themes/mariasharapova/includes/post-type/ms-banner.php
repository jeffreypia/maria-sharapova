<?php

add_action( 'init', 'register_cpt_banner' );

function register_cpt_banner() {

    $labels = array( 
        'name' => _x( 'Banner', 'ms_banner' ),
        'singular_name' => _x( 'Banner', 'ms_banner' ),
        'add_new' => _x( 'Add New Banner', 'ms_banner' ),
        'all_items' => _x( 'Banners', 'ms_banner' ),
        'add_new_item' => _x( 'Add New Banner', 'ms_banner' ),
        'edit_item' => _x( 'Edit Banner', 'ms_banner' ),
        'new_item' => _x( 'New Banner', 'ms_banner' ),
        'view_item' => _x( 'View Banner', 'ms_banner' ),
        'search_items' => _x( 'Search Banners', 'ms_banner' ),
        'not_found' => _x( 'No Banners found', 'ms_banner' ),
        'not_found_in_trash' => _x( 'No Banners found in Trash', 'ms_banner' ),
        'parent_item_colon' => _x( 'Parent Banner:', 'ms_banner' ),
        'menu_name' => _x( 'Banner', 'ms_banner' )
    );

    $supports = array(
        'title',
        'thumbnail',
        'page-attributes',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 9,
    );

    register_post_type( 'ms_banner', $args );
}
