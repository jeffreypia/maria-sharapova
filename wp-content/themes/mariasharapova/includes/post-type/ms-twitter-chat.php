<?php

add_action( 'init', 'register_cpt_ms_twitter_chat' );

function register_cpt_ms_twitter_chat() {

    $labels = array( 
        'name' => _x( 'Twitter Chat', 'ms_twitter_chat' ),
        'singular_name' => _x( 'Twitter Chat', 'ms_twitter_chat' ),
        'add_new' => _x( 'Add New Twitter Chat', 'ms_twitter_chat' ),
        'all_items' => _x( 'Twitter Chats', 'ms_twitter_chat' ),
        'add_new_item' => _x( 'Add New Twitter Chat', 'ms_twitter_chat' ),
        'edit_item' => _x( 'Edit Twitter Chat', 'ms_twitter_chat' ),
        'new_item' => _x( 'New Twitter Chat', 'ms_twitter_chat' ),
        'view_item' => _x( 'View Twitter Chat', 'ms_twitter_chat' ),
        'search_items' => _x( 'Search Twitter Chats', 'ms_twitter_chat' ),
        'not_found' => _x( 'No Twitter Chats found', 'ms_twitter_chat' ),
        'not_found_in_trash' => _x( 'No Twitter Chats found in Trash', 'ms_twitter_chat' ),
        'parent_item_colon' => _x( 'Parent Twitter Chat:', 'ms_twitter_chat' ),
        'menu_name' => _x( 'Twitter Chat', 'ms_twitter_chat' )
    );

    $supports = array(
        'title',
   );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-twitter',
    );

    register_post_type( 'ms_twitter_chat', $args );
}