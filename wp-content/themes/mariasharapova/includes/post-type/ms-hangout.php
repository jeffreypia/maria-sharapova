<?php

add_action( 'init', 'register_cpt_ms_hangout' );

function register_cpt_ms_hangout() {

    $labels = array( 
        'name' => _x( 'Hangout', 'ms_hangout' ),
        'singular_name' => _x( 'Hangout', 'ms_hangout' ),
        'add_new' => _x( 'Add New Hangout', 'ms_hangout' ),
        'all_items' => _x( 'Hangouts', 'ms_hangout' ),
        'add_new_item' => _x( 'Add New Hangout', 'ms_hangout' ),
        'edit_item' => _x( 'Edit Hangout', 'ms_hangout' ),
        'new_item' => _x( 'New Hangout', 'ms_hangout' ),
        'view_item' => _x( 'View Hangout', 'ms_hangout' ),
        'search_items' => _x( 'Search Hangouts', 'ms_hangout' ),
        'not_found' => _x( 'No Hangouts found', 'ms_hangout' ),
        'not_found_in_trash' => _x( 'No Hangouts found in Trash', 'ms_hangout' ),
        'parent_item_colon' => _x( 'Parent Hangout:', 'ms_hangout' ),
        'menu_name' => _x( 'Hangout', 'ms_hangout' )
    );

    $supports = array(
        'title',
   );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-googleplus',
    );

    register_post_type( 'ms_hangout', $args );
}