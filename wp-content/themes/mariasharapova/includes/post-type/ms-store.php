<?php

add_action( 'init', 'register_cpt_store' );

function register_cpt_store() {

    $labels = array( 
        'name' => _x( 'Store', 'ms_store' ),
        'singular_name' => _x( 'Store', 'ms_store' ),
        'add_new' => _x( 'Add New Store', 'ms_store' ),
        'all_items' => _x( 'Stores', 'ms_store' ),
        'add_new_item' => _x( 'Add New Store', 'ms_store' ),
        'edit_item' => _x( 'Edit Store', 'ms_store' ),
        'new_item' => _x( 'New Store', 'ms_store' ),
        'view_item' => _x( 'View Store', 'ms_store' ),
        'search_items' => _x( 'Search Stores', 'ms_store' ),
        'not_found' => _x( 'No Stores found', 'ms_store' ),
        'not_found_in_trash' => _x( 'No Stores found in Trash', 'ms_store' ),
        'parent_item_colon' => _x( 'Parent Store:', 'ms_store' ),
        'menu_name' => _x( 'Store', 'ms_store' ),
    );

    $supports = array(
        'title',
		'editor',
        'thumbnail',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'stores'),
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 8,
        'menu_icon' => 'dashicons-cart',
    );

    register_post_type( 'ms_store', $args );
}