<?php

add_action( 'init', 'register_cpt_partner' );

function register_cpt_partner() {

    $labels = array( 
        'name' => _x( 'Partner', 'ms_partner' ),
        'singular_name' => _x( 'Partner', 'ms_partner' ),
        'add_new' => _x( 'Add New Partner', 'ms_partner' ),
        'all_items' => _x( 'Partner', 'ms_partner' ),
        'add_new_item' => _x( 'Add New Partner', 'ms_partner' ),
        'edit_item' => _x( 'Edit Partner', 'ms_partner' ),
        'new_item' => _x( 'New Partner', 'ms_partner' ),
        'view_item' => _x( 'View Partner', 'ms_partner' ),
        'search_items' => _x( 'Search Partner', 'ms_partner' ),
        'not_found' => _x( 'No Partner found', 'ms_partner' ),
        'not_found_in_trash' => _x( 'No Partner found in Trash', 'ms_partner' ),
        'parent_item_colon' => _x( 'Parent Store:', 'ms_partner' ),
        'menu_name' => _x( 'Partner', 'ms_partner' ),
    );

    $supports = array(
        'title',
		'editor',
        'thumbnail',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 80,
        'menu_icon' => 'dashicons-businessman',
    );

    register_post_type( 'ms_partner', $args );
}