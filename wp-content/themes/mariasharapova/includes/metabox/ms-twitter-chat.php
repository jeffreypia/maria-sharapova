<?php
$prefix = 'ms_twitter_chat_';

$fields = array(
	
	array( // Text Input
		'label'	=> 'Sub-title', // <label>
		'id'	=> $prefix.'subtitle', // field id and name
		'type'	=> 'text' // type of field
	),

	array( // Text Input
		'label'	=> 'Description', // <label>
		'id'	=> $prefix.'description', // field id and name
		'type'	=> 'text' // type of field
	),

	array( // Text Input
		'label'	=> 'Link', // <label>
		'desc'	=> 'Link to Chat Page', // description
		'id'	=> $prefix.'link', // field id and name
		'type'	=> 'text' // type of field
	),
);

/**
 * Instantiate the class with all variables to create a meta box
 * var $id string meta box id
 * var $title string title
 * var $fields array fields
 * var $page string|array post type to add meta box to
 * var $js bool including javascript or not
 */
$sample = new custom_add_meta_box( 'twitter_chat', 'Chat Details', $fields, array('ms_twitter_chat'), true );

?>
