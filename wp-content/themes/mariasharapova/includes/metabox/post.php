<?php
$prefix = 'post_';

$fields = array(
	
	array( // Desktop thumbnail image
		'label'	=> 'Desktop Featured Image', // <label>
		'desc'	=> '940x470 - image appears on desktop view', // description
		'id'	=> $prefix.'desktop_thumbnail', // field id and name
		'type'	=> 'image' // type of field
	),
);

/**
 * Instantiate the class with all variables to create a meta box
 * var $id string meta box id
 * var $title string title
 * var $fields array fields
 * var $page string|array post type to add meta box to
 * var $js bool including javascript or not
 */
$sample = new custom_add_meta_box( 'post_meta', 'Post Details', $fields, array('post'), true );

?>
