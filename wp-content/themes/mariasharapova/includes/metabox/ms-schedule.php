<?php
$prefix = 'ms_schedule_';

$fields = array(
	
	array( // Icon
		'label'	=> 'Icon', // <label>
		'desc'	=> '75x75 - if icon is not a square, please make sure it is saved as a transparent PNG.', // description
		'id'	=> $prefix.'icon', // field id and name
		'type'	=> 'image' // type of field
	),

	array( // Text Input
		'label'	=> 'Type', // <label>
		'desc'	=> 'Event type', // description
		'id'	=> $prefix.'type', // field id and name
		'type'	=> 'text' // type of field
	),

	array( // Text Input
		'label'	=> 'Location', // <label>
		'desc'	=> 'Event location', // description
		'id'	=> $prefix.'location', // field id and name
		'type'	=> 'text' // type of field
	),

	array( // Text Input
		'label'	=> 'Date', // <label>
		'desc'	=> 'Event date', // description
		'id'	=> $prefix.'date', // field id and name
		'type'	=> 'text' // type of field
	),

	array( // Text Input
		'label'	=> 'Link', // <label>
		'desc'	=> 'Link to event details', // description
		'id'	=> $prefix.'link', // field id and name
		'type'	=> 'text' // type of field
	),
);

/**
 * Instantiate the class with all variables to create a meta box
 * var $id string meta box id
 * var $title string title
 * var $fields array fields
 * var $page string|array post type to add meta box to
 * var $js bool including javascript or not
 */
$sample = new custom_add_meta_box( 'schedule', 'Event Details', $fields, array('ms_schedule'), true );

?>
