<?php
$prefix = 'gallery_';

$fields = array(
	array( // Repeatable & Sortable Text inputs
		'label'	=> 'Gallery Images', // <label>
		// 'desc'	=> 'Images to appear in this page\'s gallery. (Only applicable if gallery was built for this page)', // description
		'id'	=> $prefix.'repeatable', // field id and name
		'type'	=> 'repeatable', // type of field
		'sanitizer' => array( // array of sanitizers with matching kets to next array
			'featured' => 'meta_box_santitize_boolean',
			'title' => 'sanitize_text_field',
			'desc' => 'wp_kses_data'
		),
		'repeatable_fields' => array ( // array of fields to be repeated
			array( // Image ID field
				'label'	=> 'Gallery Image', // <label>
				// 'desc'  => 'Gallery Image', // description
				'id'	=> 'image', // field id and name
				'type'	=> 'image' // type of field
			)
		)
	),
);

/**
 * Instantiate the class with all variables to create a meta box
 * var $id string meta box id
 * var $title string title
 * var $fields array fields
 * var $page string|array post type to add meta box to
 * var $js bool including javascript or not
 */
$sample = new custom_add_meta_box( 'gallery', 'Gallery', $fields, array('page'), true );

?>
