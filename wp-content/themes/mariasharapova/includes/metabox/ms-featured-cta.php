<?php
$prefix = 'ms_featured_';

$fields = array(
	
	array( // Desktop thumbnail image
		'label'	=> 'Desktop Thumbnail', // <label>
		'desc'	=> '781x390 - image appears on desktop view', // description
		'id'	=> $prefix.'desktop_thumbnail', // field id and name
		'type'	=> 'image' // type of field
	),

	array( // Text Input
		'label'	=> 'Link (optional)', // <label>
		'desc'	=> 'Link for Featured CTA', // description
		'id'	=> $prefix.'link', // field id and name
		'type'	=> 'text' // type of field
	),
);

/**
 * Instantiate the class with all variables to create a meta box
 * var $id string meta box id
 * var $title string title
 * var $fields array fields
 * var $page string|array post type to add meta box to
 * var $js bool including javascript or not
 */
$sample = new custom_add_meta_box( 'featured', 'Featured CTA Details', $fields, array('ms_featured'), true );

?>
