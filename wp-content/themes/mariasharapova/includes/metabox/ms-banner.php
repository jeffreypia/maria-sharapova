<?php
$prefix = 'ms_banner_';

$fields = array(
	
	array( // Text Input
		'label'	=> 'Subheading', // <label>
		'desc'	=> 'Subheading', // description
		'id'	=> $prefix.'subheading', // field id and name
		'type'	=> 'text' // type of field
	),

	// Using page order field instead
	// array( // Text Input
	// 	'label'	=> 'Position (optional)', // <label>
	// 	'desc'	=> 'Position ad will appear (ensure this is less than the number of posts that appear under Settings > Reading)', // description
	// 	'id'	=> $prefix.'position', // field id and name
	// 	'type'	=> 'text' // type of field
	// ),
);

/**
 * Instantiate the class with all variables to create a meta box
 * var $id string meta box id
 * var $title string title
 * var $fields array fields
 * var $page string|array post type to add meta box to
 * var $js bool including javascript or not
 */
$sample = new custom_add_meta_box( 'banner', 'Sponsor details', $fields, array('ms_banner'), true );

?>
