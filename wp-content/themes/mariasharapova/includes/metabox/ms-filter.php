<?php
$prefix = 'ms_filter_';

$fields = array(
	
	array(
	    'label' => 'Chosen Select Box',
	    'desc'  => 'Please Select what type of data you want to query',
	    'id'    => $prefix . 'select',
	    'type'  => 'chosen',
	    'multiple' => false,
	    'options' => array (
	    	'all' => array (
	            'label' => 'All',
	            'value' => 'all'
	        ),
	        'category' => array (
	            'label' => 'Category',
	            'value' => 'category'
	        ),
	        'post_type' => array (
	            'label' => 'Post Type',
	            'value' => 'post_type'
	        ),
	        'post_format' => array (
	            'label' => 'Post Format',
	            'value' => 'post_format'
	        )
	    )
	 ),
	 array( // Text Input
	 	'label'	=> 'Value', // <label>
	 	'desc'	=> 'Please comma separate values and use IDs for Categories!', // description
	 	'id'	=> $prefix.'value', // field id and name
	 	'type'	=> 'text' // type of field
	 ), 	


	

	// Using page order field instead
	// array( // Text Input
	// 	'label'	=> 'Position (optional)', // <label>
	// 	'desc'	=> 'Position ad will appear (ensure this is less than the number of posts that appear under Settings > Reading)', // description
	// 	'id'	=> $prefix.'position', // field id and name
	// 	'type'	=> 'text' // type of field
	// ),
);

/**
 * Instantiate the class with all variables to create a meta box
 * var $id string meta box id
 * var $title string title
 * var $fields array fields
 * var $page string|array post type to add meta box to
 * var $js bool including javascript or not
 */



$sample = new custom_add_meta_box( 'filter', 'Filter details', $fields, array('ms_filter'), true );

?>


