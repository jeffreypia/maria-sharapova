<?php
	// Loop of Ad posts
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'			=> array( 'ms_ad' ),
		'orderby'			=> 'menu_order',	// ads need to be injected ASC order
		'order'				=> 'ASC',			// else they will push each other
	);
	$ads = get_posts( $args );
	$foundPosts = count($posts);

	// Inject ads into main loop specified Menu Order positions
	// If Menu Order is not specified (0 by default), 
	// order will be randomly assigned
	foreach( $ads as $ad ) :
		$pos = $ad->menu_order;
		$pos = $pos ? $pos : rand(1, $foundPosts);
		array_splice($posts, $pos-1, 0, array( $ad ));
	endforeach;
?>