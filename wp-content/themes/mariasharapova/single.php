<?php
/**
 * The Template for displaying all single posts
 */

get_header(); 
	
	while ( have_posts() ) : the_post();
		$banner = lm_get_img_src('post_desktop_thumbnail', 'full');
		$bannerMobile = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		$gallery = has_post_format( 'gallery' );
?>
		
			<section id="contentArticle" class="<?php echo $gallery ? 'content-gallery' : 'content-article' ?>" role="main">

			<?php if( $banner || $bannerMobile ) : ?>
			<figure class="banner">
				<img src="" data-full="<?php echo $banner ?>" data-mobile="<?php echo $bannerMobile ?>" alt="">
			</figure>
			<?php endif; ?>

			<article class="entry-content">
				<header class="article-header">
					<h1 class="article-title"><?php the_title(); ?></h1>
					<time class="article-date" datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php the_date('M j, Y'); ?></time>
					<?php edit_post_link('Edit post'); ?>
				</header>
				
				<?php the_content(); ?>

				<footer class="article-footer">
					<?php get_template_part('partial', 'share'); ?>
				</footer>
			</article>

		</section><!-- #contentArticle -->

<?php
		if( $gallery ) :
			get_template_part( 'partial', 'modal' );
		else :
			get_sidebar('posts');
		endif;

	endwhile;
?>

	<button id="top" class="top">Back to top</button>

<?php get_footer();