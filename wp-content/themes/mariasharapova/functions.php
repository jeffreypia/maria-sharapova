<?php
/**
 * Maria Sharapova functions and definitions
 */

// base path
$base = get_template_directory().'/includes';

/* Helpers */
require_once("$base/util.php");

// Register post type
require_once("$base/post-type/ms-featured-cta.php");
require_once("$base/post-type/ms-schedule.php");
require_once("$base/post-type/ms-ad.php");
require_once("$base/post-type/ms-hangout.php");
require_once("$base/post-type/ms-twitter-chat.php");
require_once("$base/post-type/ms-sponsor.php");
require_once("$base/post-type/ms-store.php");
require_once("$base/post-type/ms-store-item.php");
require_once("$base/post-type/ms-filter.php");
require_once("$base/post-type/ms-banner.php");
require_once("$base/post-type/ms-partner.php");

/* Classes */
require_once("$base/class/metaboxes/meta_box.php");

/* Metaboxes */
require_once("$base/metabox/page.php");
require_once("$base/metabox/post.php");
require_once("$base/metabox/ms-featured-cta.php");
require_once("$base/metabox/ms-schedule.php");
require_once("$base/metabox/ms-ad.php");
require_once("$base/metabox/ms-hangout.php");
require_once("$base/metabox/ms-twitter-chat.php");
require_once("$base/metabox/ms-sponsor.php");
require_once("$base/metabox/ms-store-item.php");
require_once("$base/metabox/ms-banner.php");
require_once("$base/metabox/ms-partner.php");

if ( ! isset( $content_width ) ) {
	$content_width = 600;
}

if ( ! function_exists( 'mariasharapova_setup' ) ) :

function mariasharapova_setup() {

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );


	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => __( 'Primary menu', 'mariasharapova' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'gallery',
		'video',
	) );
}
endif; // mariasharapova_setup
add_action( 'after_setup_theme', 'mariasharapova_setup' );

/**
 * Register three widget areas.
 */
function mariasharapova_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'mariasharapova' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Main sidebar that appears on the left.', 'mariasharapova' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'mariasharapova_widgets_init' );

/**
 * Modify main loop
 */
function mariasharapova_alter_main_query($query){
	// Only alter main query
	if ( !$query->is_main_query() )
		return;

	//  Show custom post types on home page
	if ( $query->is_home() ) {
		$query->set( 'post_type', array( 'ms_schedule', 'ps_facebook', 'ps_twitter', 'ps_instagram', 'post', 'ms_store', 'ms_partner') );
		$query->set( 'posts_per_page', 14 );
	}
}
add_action('pre_get_posts','mariasharapova_alter_main_query');

function mariasharapova_alter_menu () {
    global $menu;
	// Positions for Core Menu Items

	// 2 Dashboard
	// 4 Separator
	// 5 Posts
	// 10 Media
	// 15 Links
	// 20 Pages
	// 25 Comments
	// 59 Separator
	// 60 Appearance
	// 65 Plugins
	// 70 Users
	// 75 Tools
	// 80 Settings
	// 99 Separator

    // Removing unused menu items
    $menu[19] = $menu[10]; // Move Media
    unset($menu[10]); // Remove Media
    unset($menu[15]); // Remove Links
    unset($menu[25]); // Remove Comments
}
add_action('admin_menu', 'mariasharapova_alter_menu');

// Enqueue scripts the right way!
function mariasharapova_register_scripts() {
	// Use Google's CDN for jQuery
	wp_deregister_script('jquery');
	wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js", false, '2.1.0', false);

	// wp_deregister_script('jquery-ui');
	// wp_register_script('jquery-ui', 'http' . ($_SERVER['SERVER_PORT'] == 443 ? 's' : '') . '://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js', array('jquery'), '1.8.6');

	// Loading plugins from within siteFunctions - 2014-04-25 JP
	// wp_enqueue_script('plugins', get_template_directory_uri() . '/assets/js/plugins.js', array('jquery'), false, true);
	wp_enqueue_script('scripts', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), false, true);

	wp_enqueue_script('twitter', 'http' . ($_SERVER['SERVER_PORT'] == 443 ? 's' : '') . '://platform.twitter.com/widgets.js', false, false, true);
}
if (!is_admin()) add_action("wp_enqueue_scripts", "mariasharapova_register_scripts", 11);

// Change excerpt length
function mariasharapova_excerpt_length($length) {
    return 5;
}
add_filter('excerpt_length', 'mariasharapova_excerpt_length');

// Change excerpt ellipsis
function mariasharapova_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter('excerpt_more', 'mariasharapova_excerpt_more');

// Pull FB Like count
function mariasharapova_fb_like_count() {
	// $temp = "";
	// $options = get_option('facebook_sync_option');
	// if (is_array($options)) :
	// 	if (!class_exists('Facebook')) require_once("$base/vendor/facebook/facebook.php");
	// endif;

	// $config = array(
	// 	'appId' => $options['app_id'], // App ID from above
	// 	'secret' => $options['secret'], // Secret from above
	// 	'fileUpload' => false, // optional
	// 	'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
	// );

	// if (!isset($fb) && class_exists('Facebook') && is_string($options['fb_page'])) :
	// 	$fb = new Facebook($config); // Generates FB object
	// 	$path = '/'.$options['fb_page'].'/';
	// 	$response = $fb->api($path);
	// endif;

	// if(is_array($response) && is_string($options['fb_page']) && isset($response['likes'])) :
	// 	$temp = '<a href="http://facebook.com/' . $options['fb_page'] . '" title="" class="fb-like-count" target="_blank">' . number_format( $response['likes'] ) . '</a>';
	// endif;

	// return $temp;
}

// Pull Twitter Follower count
function mariasharapova_twitter_follower_count() {
	$options = get_option('twitter_sync_option');
	if($options) :
		if( !class_exists('TwitterAPIExchange') ) require_once("$base/vendor/twitter/TwitterAPIExchange.php");

		$settings = array(
			'oauth_access_token'		=> $options['access_token'],
			'oauth_access_token_secret'	=> $options['access_token_secret'],
			'consumer_key'				=> $options['api_key'],
			'consumer_secret'			=> $options['api_secret']
		);

		$twitter = new TwitterAPIExchange($settings);

		// Query Twitter for tweets after most recent posted tweet
		$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
		$getfield = '?screen_name='.$options['twitter_username'];
		$requestMethod = 'GET';

		$json = $twitter->setGetfield($getfield)
				->buildOauth($url, $requestMethod)
				->performRequest();
		$response = json_decode($json);
	endif;

	if(is_array($response)) {

		$temp = '<a href="http://twitter.com/' . $options['twitter_username'] . '" title="" class="twitter-follower-count" target="_blank">' . number_format( $response[0]->user->followers_count ) . '</a>';

	} else {

		return false;

	}


	return $temp;
}

// Fix for home page title
add_filter( 'wp_title', 'mariasharapova_wp_title_for_home' );
function mariasharapova_wp_title_for_home( $title)
{
	$title .= get_bloginfo( 'name', 'display' );

	return $title;
}

// Customize photos gallery
add_filter('post_gallery', 'mariasharapova_post_gallery', 10, 2);
function mariasharapova_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) :
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby']) unset($attr['orderby']);
    endif;

    extract(shortcode_atts(array(
        'order' 		=> 'ASC',
        'orderby' 		=> 'menu_order ID',
        'id' 			=> $post->ID,
        'itemtag' 		=> 'dl',
        'icontag' 		=> 'dt',
        'captiontag' 	=> 'dd',
        'columns' 		=> 3,
        'size' 			=> 'thumbnail',
        'include' 		=> '',
        'exclude' 		=> ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) :
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array(
        	'include' => $include,
        	'post_status' => 'inherit',
        	'post_type' => 'attachment',
        	'post_mime_type' => 'image',
        	'order' => $order,
        	'orderby' => $orderby
        ));

        $attachments = array();
        foreach ($_attachments as $key => $val) :
            $attachments[$val->ID] = $_attachments[$key];
        endforeach;
    endif;

    if (empty($attachments)) return '';

    // Here's your actual output, you may customize it to your need
    $output = "<ul id=\"galleryPhotos\" class=\"gallery-photos\">\n";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) :
        // Fetch the thumbnail (or full image, it's up to you)
        $thumbnail = wp_get_attachment_image_src($id, 'thumbnail');
        $img = wp_get_attachment_image_src($id, 'full');

        $output .= "<li class=\"gallery-photo\">\n";
        $output .= "	<a class=\"entry-thumbnail lightbox\" href=\"{$img[0]}\" title=\"{$attachment->post_title}\" data-excerpt=\"{$attachment->post_excerpt}\">\n";
        $output .= "		<img src=\"{$thumbnail[0]}\" alt=\"{$attachment->post_title}\">\n";
        $output .= "	</a>\n";
        $output .= "</li>\n";
    endforeach;

    $output .= "</ul>\n";

    return $output;
}

add_action( 'do_meta_boxes', 'mariasharapova_do_meta_boxes' );
function mariasharapova_do_meta_boxes( $post_type ) {
	global $wp_meta_boxes;
	if ( ! current_theme_supports( 'post-thumbnails', $post_type ) || ! post_type_supports( $post_type, 'thumbnail' ) )
		return;

	foreach ( $wp_meta_boxes[ $post_type ] as $context => $priorities )
		foreach ( $priorities as $priority => $meta_boxes )
			foreach ( $meta_boxes as $id => $box )
				if ( 'postimagediv' == $id )
					$wp_meta_boxes[ $post_type ][ $context ][ $priority ][ $id ]['title'] .= ' 470w x 470h';

	remove_action( 'do_meta_boxes', 'mariasharapova_do_meta_boxes' );
}

// get current paged item
function get_paged(){
    global $paged;
    if( get_query_var( 'paged' ) )
        $my_page = get_query_var( 'paged' );
    else {
        if( get_query_var( 'page' ) )
            $my_page = get_query_var( 'page' );
        else
            $my_page = 1;
        set_query_var( 'paged', $my_page );
        $paged = $my_page;
    }
    return $my_page;
}

