<?php 
	// Get most recent Schedule post
	$args = array(
		'posts_per_page'	=> 1,
		'post_type'			=> array( 'ms_schedule' ),
	);
	$featured = get_posts( $args );
	global $skipID; // to be used in main loop to ensure post is not duplicated

	foreach( $featured as $post ) : setup_postdata($post);

		array_splice($posts, 1, 0, array( $post ));
		// get_template_part( 'posttype', 'ms_schedule' );
		$skipID = $post->ID;

	endforeach;
