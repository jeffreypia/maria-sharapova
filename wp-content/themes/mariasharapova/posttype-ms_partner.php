<?php 
	// Schedule post template

	$postBg = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	$postIcon = lm_get_img_src('ms_partner_icon', 'full');

	$args = array(
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'post_type' => 'ms_partner',
		'post_status' => 'publish',
	);

		
	?>


<div <?php post_class('loading'); ?> id="<?php echo $post->post_name; ?>">

			<?php if( $postBg ) : ?>
			<div class="entry-thumbnail">
				<noscript><img src="<?php echo $postBg; ?>" alt=""></noscript>
				<img src="" data-src="<?php echo $postBg; ?>" alt="" class="lazy">
			</div>
			<?php endif; ?>
			
			<div class="content-wrapper">
				<h3 class="entry-category">Partner</h3>
				<img class="entry-icon" src="<?php echo $postIcon; ?>" alt="">
				<h2 class="entry-title"><?php the_title(); ?></h2>

				<?php edit_post_link('Edit post'); ?>

				<div class="button-container ajax">
					<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
				</div>

			</div><!-- .content-wrapper -->

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
