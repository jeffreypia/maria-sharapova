<?php
	// Template for Twitter Chat

	// Get Twitter Chat post
	$args = array(
		'posts_per_page'	=> 1,
		'post_type'			=> array( 'ms_twitter_chat' ),
	);
	$chat = get_posts( $args );

	foreach( $chat as $post ) : setup_postdata($post);
		$postSubtitle = lm_get('ms_twitter_chat_subtitle');
		$postDescription = lm_get('ms_twitter_chat_description');
		$postLink = lm_get('ms_twitter_chat_link');
		if( $postLink ) :
?>

		<div <?php post_class('no-filter loading'); ?> id="<?php echo $post->post_name; ?>">

			<div class="content-wrapper">
				<div class="entry-date">
					<div class="entry-month"><?php echo get_the_date('M'); ?></div>
					<div class="entry-day"><?php echo get_the_date('j'); ?></div>
				</div><!-- .entry-date -->

				<?php if( $postSubtitle ) : ?>
				<h3 class="entry-category"><?php echo $postSubtitle; ?></h3>
				<?php endif; ?>

				<?php if( $postDescription ) : ?>
				<h2 class="entry-title"><?php echo $postDescription; ?></h2>
				<?php endif; ?>

				<?php edit_post_link('Edit post'); ?>

				<div class="button-container ajax">
					<a target="_blank" href="<?php echo $postLink; ?>" class="read-more">Join Chat Now</a>
				</div>

			</div><!-- .content-wrapper -->

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
<?php
		endif;
	endforeach;
?>