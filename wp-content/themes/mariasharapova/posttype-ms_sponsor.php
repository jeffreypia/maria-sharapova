<?php 
	// Template for Sponsor Posts
	//$postLink = lm_get('ms_sponsor_link');
	$postBg = get_field('sponsor_image');
	$categories = get_the_category(); 
	$category = $categories[0]->name;
	$postFormat = get_post_format();
	$postFormat = trim($postFormat);
	$buttonText = "Read More";

?>
	<?php if( $postBg ) : ?>
		<div <?php post_class('loading sponsor-post'); ?>>
			<a href="<?php the_permalink(); ?>" class="entry-thumbnail">
				<noscript><img src="<?php the_field('sponsor_image'); ?>" alt=""></noscript>
				<img src="" data-src="<?php the_field('sponsor_image'); ?>" alt="" class="lazy">
			</a>
			
			<!-- <div class="content-wrapper"> -->
			<!-- <h3 class="entry-category"><?php echo $category; ?></h3> -->
			<!-- <h2 class="entry-title"><?php the_title(); ?></h2> -->

			<?php edit_post_link('Edit post'); ?>

			<!-- </div>.content-wrapper -->

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
	<?php endif; ?>