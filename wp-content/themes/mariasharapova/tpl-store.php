<?php
/**
 *  Template Name: Store Page
 */

get_header(); 


$args = array(
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'post_type' => 'ms_store',
	'post_status' => 'publish',
);

$posts = get_posts( $args );

	while ( have_posts() ) : the_post();

	$shopItemIDs = rpt_get_object_relation($posts[0]->ID, 'ms_store_item');
	if ( $shopItemIDs ) {
		$args = array(
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'post_type' => 'ms_store_item',
			'post_status' => 'publish',
			'post__in' => $shopItemIDs,
		);

		$shopItems = get_posts( $args );
	}
	
?>

	<section id="contentArticle" class="content-article store" role="main">

				<article class="partner-post has-store dynamic-store posts">
					<?php
						foreach ( $posts as $post ) : setup_postdata( $post );
							get_template_part( 'posttype', 'ms_store' );
						endforeach;
						?>
				</article>
				<?php 

				$post = $posts[0];
				get_template_part( 'loop', 'store-items' ); ?>

	</section><!-- #contentMain -->

	<?php endwhile; ?>

<?php get_footer();?>