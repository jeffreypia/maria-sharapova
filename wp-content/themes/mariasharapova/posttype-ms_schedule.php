<?php 
	// Schedule post template

	$postBg = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

	$postIcon = lm_get_img_src('ms_schedule_icon', 'full');
	$postType = lm_get('ms_schedule_type');
	$postType = $postType ? $postType : 'Event';
	$postLocation = lm_get('ms_schedule_location');
	$postEventDate = lm_get('ms_schedule_date');
	$postLink = lm_get('ms_schedule_link');

	// Checking if Schedule Link is internal or external
	$url_host = parse_url( $postLink, PHP_URL_HOST );
	$base_url_host = parse_url( get_permalink($post->ID), PHP_URL_HOST );
	$linkIsInternal = ( $url_host == $base_url_host || empty($url_host) );
?>
		<div <?php post_class('loading'); ?> id="<?php echo $post->post_name; ?>">

			<?php if( $postBg ) : ?>
			<div class="entry-thumbnail">
				<noscript><img src="<?php echo $postBg; ?>" alt=""></noscript>
				<img src="" data-src="<?php echo $postBg; ?>" alt="" class="lazy">
			</div>
			<?php endif; ?>
			
			<div class="content-wrapper">

				<?php if( $postIcon ) : ?>
				<img class="entry-icon" src="<?php echo $postIcon; ?>" alt="">
				<?php else : ?>
				<div class="entry-date">
					<div class="entry-month"><?php echo get_the_date('M'); ?></div>
					<div class="entry-day"><?php echo get_the_date('j'); ?></div>
				</div>
				<?php endif; ?>

				<h3 class="entry-category">Upcoming <?php echo $postType; ?></h3>

				<h2 class="entry-title"><?php the_title(); ?></h2>

				<?php if ( $postLocation ) { ?><div class="entry-location"><?php echo $postLocation; ?></div><?php } ?>

				<?php if ( $postEventDate ) { ?><div class="entry-event-date"><?php echo $postEventDate; ?></div><?php } ?>

				<?php edit_post_link('Edit post'); ?>

				<?php if ( $postLink ) : ?>
				<div class="button-container">
					<a <?php echo $linkIsInternal ? '' : 'target="_blank"' ?> href="<?php echo $postLink; ?>" class="read-more">View <?php echo $postType; ?></a>
				</div>
				<?php endif; ?>

			</div><!-- .content-wrapper -->

			<?php get_template_part( 'partial', 'throbber' ); ?>
		</div>
